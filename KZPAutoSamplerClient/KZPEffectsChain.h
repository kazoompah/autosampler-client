//
//  KZPEffectsChain.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 20/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

@import AVFoundation;

/*
 
    Effects chain will be 'blank' if no effects are applied
 
    If it is blank, it just returns nil for its input and output nodes (then the
    caller will know to hook the buffer player straight into the mixer)
 
    If it has only one effect, it will return the same node for both input and output
 
 */

// TODO: Will absolutely need an enforced ordering of effects, unlike a DAW

// E.g. Pitch/Stretch/Speed < Bassify < Ringmod/Crush < Delay < Reverb
//      KZPEffectOrderingMin    +1         +2            +3     KZPEffectOrderingMax

@protocol KZPEffectsChainDelegate <NSObject>

- (void)effectsChainNeedsReconnecting;

@end

@interface KZPEffectsChain : NSObject

@property (weak, nonatomic) id<KZPEffectsChainDelegate> delegate;

@property (strong, nonatomic) AVAudioFormat *format;

- (AVAudioNode *)effectsChainInputNode;
- (AVAudioNode *)effectsChainOutputNode;

- (NSArray *)activeEffects; // Array of effect IDs
- (NSDictionary *)currentSettings;
- (void)loadWithDictionary:(NSDictionary *)effectSettings withCompletion:(void (^)(void))completionBlock;

- (void)applyContinuousValue:(float)value toEffect:(NSString *)effectID;
- (float)continuousValueForEffect:(NSString *)effectID;
- (void)activateEffect:(NSString *)effectID withoutReconnecting:(BOOL)noReconnection;
- (void)deactivateEffect:(NSString *)effectID withoutReconnecting:(BOOL)noReconnection;

- (void)copyEffectsChain:(KZPEffectsChain *)effectsChain withCompletion:(void (^)(void))completionBlock;

@end
