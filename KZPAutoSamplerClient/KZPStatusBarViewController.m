//
//  KZPStatusBarViewController.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 26/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPStatusBarViewController.h"
#import "KZPStatusBarView.h"
#import "KZPRemoteSampleServer.h"

@interface KZPStatusBarViewController ()  <KZPServerConnectionDelegate>

@property (weak, nonatomic) KZPStatusBarView *statusView;
@property (strong, nonatomic) UIColor *errorColor;
@property (strong, nonatomic) UIColor *successColor;
@property (strong, nonatomic) UIColor *issueColor;

@end

@implementation KZPStatusBarViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    _statusView = (KZPStatusBarView *)[self view];
    [[KZPRemoteSampleServer sharedServer] setConnectionDelegate:self];
    
    _errorColor = [UIColor colorWithRed:218.0/255.0 green:1.0/255.0 blue:31.0/255.0 alpha:1.0];
    _successColor = [UIColor colorWithRed:50.0/255.0 green:230.0/255.0 blue:16.0/255.0 alpha:1.0];
    _issueColor = [UIColor colorWithRed:249.0/255.0 green:178.0/255.0 blue:29.0/255.0 alpha:1.0];
    
    _statusView.layer.shadowRadius = 4;
    _statusView.layer.shadowOpacity = 0.3;
    _statusView.layer.masksToBounds = NO;
    _statusView.layer.shadowOffset = CGSizeMake(0, 3);
}

- (void)connectionWasEstablished
{
    [_statusView changeStatusViewColor:_successColor];
    [_statusView hideStatusLabel];
}

- (void)connectionWasMaintained
{
    [_statusView registerPoll];
}

- (void)connectionWarningWithMessage:(NSString *)warningMessage
{
    [_statusView changeStatusViewColor:_issueColor];
    [_statusView updateStatusLabel:warningMessage];
}

- (void)connectionFailedWithMessage:(NSString *)failureMessage
{
    [_statusView changeStatusViewColor:_errorColor];
    [_statusView updateStatusLabel:failureMessage];
    [_statusView hidePollIndicator];
}

- (void)connectionWasPaused
{
    [_statusView hidePollIndicator];
}

@end
