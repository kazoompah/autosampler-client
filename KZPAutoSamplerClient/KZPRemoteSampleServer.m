//
//  KZPRemoteSampleServer.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 26/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPRemoteSampleServer.h"
#import "KZPServerDiscoveryService.h"
#import "KZPServerQueryService.h"

static KZPRemoteSampleServer *sharedInstance;

@interface KZPRemoteSampleServer () <KZPServerDiscoveryServiceDelegate, KZPServerQueryServiceDelegate>

@property (strong, nonatomic) NSString *host;
@property (nonatomic) uint16_t port;

@property (strong, nonatomic) KZPServerDiscoveryService *discoveryService;
@property (strong, nonatomic) KZPServerQueryService *queryService;

@end

@implementation KZPRemoteSampleServer

+ (KZPRemoteSampleServer *)sharedServer
{
    if (!sharedInstance) sharedInstance = [[KZPRemoteSampleServer alloc] init];
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _discoveryService = [[KZPServerDiscoveryService alloc] init];
        _discoveryService.delegate = self;
        _queryService = [[KZPServerQueryService alloc] init];
        _queryService.delegate = self;
    }
    return self;
}

- (void)start
{
    [_discoveryService startDiscovery];
}

- (NSString *)basePath
{
    return [NSString stringWithFormat:@"http://%@:%u", _host, _port];
}

- (KZPFileDownloader *)fileDownloaderForSampleID:(NSString *)sampleID
{
    KZPFileDownloader *downloader = [[KZPFileDownloader alloc] init];
    NSString *localFileName = [sampleID stringByAppendingPathExtension:@"wav"];
    [downloader setFileName:localFileName];
    NSString *remotePath = [[self basePath] stringByAppendingPathComponent:kSERVER_PATH_SAMPLES];
    downloader.remoteFilePath = [remotePath stringByAppendingPathComponent:sampleID];
    return downloader;
}

- (KZPFileUploader *)fileUploaderForFilename:(NSString *)filename extension:(NSString *)extension
{
    KZPFileUploader *uploader = [[KZPFileUploader alloc] init];
    NSString *localFilename = [filename stringByAppendingPathExtension:extension];
    [uploader setFilename:localFilename];
    uploader.remotePath = [[self basePath] stringByAppendingPathComponent:kSERVER_PATH_SAMPLE_PACKS];
    return uploader;
}

- (void)startQuerying
{
    [_connectionDelegate connectionWasEstablished];
    [_queryService startPolling];
}

- (void)pauseQuerying
{
    [_connectionDelegate connectionWasPaused];
    [_queryService stopPolling];
}


#pragma mark - <KZPServerDiscoveryServiceDelegate>


- (void)serverDiscoveredAtIP:(NSString *)serverIP
{
    [_discoveryService stopDiscovery];
    
    _host = serverIP;
    _port = SERVER_QUERY_TCP_PORT;      // Note: in future it might be wise for the server to return the TCP port
    
    _queryService.remoteQueryPath = [[self basePath] stringByAppendingPathComponent:kSERVER_PATH_SAMPLES_SINCE];
    
    // don't start polling until a delegate is registered to receive the results?
    [self startQuerying];
}

- (void)serverDiscoveryFailedWithMessage:(NSString *)message abort:(BOOL)abort
{
    if (abort) {
        [_discoveryService stopDiscovery];
    }
    [_connectionDelegate connectionFailedWithMessage:message];
}

- (void)serverCannotBeFound
{
    [_connectionDelegate connectionFailedWithMessage:kConnectionStatus_ServerNotFound];
}


#pragma mark - <KZPServerQueryServiceDelegate>


- (void)queryServiceLostConnection
{
    [_connectionDelegate connectionFailedWithMessage:kConnectionStatus_ServerConnectionLost];
    [_queryService stopPolling];
    [_discoveryService startDiscovery];
}

- (void)queryServiceHasConnection
{
    [_connectionDelegate connectionWasMaintained];
}

- (void)queryServiceReceivedSampleListUpdate:(NSArray *)sampleList
{
    [_queryDelegate receiveSampleList:sampleList];
}

@end
