//
//  KZPUDPDiscoveryService.h
//  KZPAutoSamplerClient
//
//  Created by Matt Rankin on 30/08/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncUdpSocket.h"

@protocol KZPUDPDiscoveryDelegate <NSObject>

- (void)discoverySucceededWithIP:(NSString *)serverIP;
- (void)discoveryFailedWithMessage:(NSString *)message abort:(BOOL)abort;

@end

@interface KZPUDPDiscoveryService : NSObject <GCDAsyncUdpSocketDelegate>

@property (weak, nonatomic) id<KZPUDPDiscoveryDelegate> delegate;

- (void)sendDiscoveryBroadcast;
- (void)cancel;

@end
