//
//  KZPSampleFileManager.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 21/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 
    This class basically exists to ensure that sample objects cannot delete
    files that other sample objects are using. It may have some other uses for
    the download process too, and it could handle file path logic.
 
    Samples being deallocated should call this class with their file paths.
 
 */

@interface KZPSampleFileManager : NSObject

+ (KZPSampleFileManager *)sharedFileManager;

- (NSString *)documentsPath;

- (void)sampleOwner:(id)owner refersToSampleAtFileURL:(NSURL *)fileURL;
- (void)sampleOwner:(id)owner attemptToRemoveSampleAtFileURL:(NSURL *)fileURL;

@end
