//
//  KZPQueueItemTableViewCell.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 18/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPQueueItemTableViewCell.h"
#import "FDWaveformView-Swift.h"


@interface KZPQueueItemTableViewCell () <FDWaveformViewDelegate>

@property (weak, nonatomic) IBOutlet FDWaveformView *waveformImageView;
@property (weak, nonatomic) IBOutlet UIButton *samplePlayButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *highlightView;

@property (strong, nonatomic) NSURL *currentAudioURL;

@end

@implementation KZPQueueItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.waveformImageView.layer.shadowRadius = 4;
    self.waveformImageView.layer.shadowOpacity = 0.3;
    self.waveformImageView.layer.masksToBounds = NO;
    self.waveformImageView.layer.shadowOffset = CGSizeMake(0, 3);
    self.waveformImageView.doesAllowScrubbing = NO;
    self.waveformImageView.doesAllowStretch = NO;
    self.waveformImageView.doesAllowScroll = NO;
    self.waveformImageView.wavesColor = [UIColor colorWithWhite:0.28 alpha:1.0];
    self.waveformImageView.delegate = self;
}

- (IBAction)playButtonPressed:(id)sender
{
    [self.delegate queueItemPlayButtonPressed];
}

- (void)reset
{
    [self displayStandbyState:NO];
    [self displayReadyState:NO];
    [self displayActiveState:NO];
    _waveformImageView.hidden = YES;
}

- (void)displayReadyState:(BOOL)ready
{
    self.waveformImageView.alpha = ready ? 1.0 : 0.25;
    ready ? [self.activityIndicator stopAnimating] : [self.activityIndicator startAnimating];
}

- (void)displayActiveState:(BOOL)active
{
    self.highlightView.backgroundColor = active ? [UIColor yellowColor] : [UIColor clearColor];
}

- (void)displayStandbyState:(BOOL)standby
{
    self.backgroundColor = standby ? [UIColor orangeColor] : [UIColor colorWithWhite:0.6 alpha:1.0];
}

- (void)displayWaveformForAudioFileURL:(NSURL *)audioFileURL
{
    if (self.currentAudioURL != audioFileURL) {
        self.waveformImageView.audioURL = audioFileURL;
        self.currentAudioURL = audioFileURL;
    }
}


#pragma mark - <FDWaveformViewDelegate>


- (void)waveformViewWillLoad:(FDWaveformView *)waveformView
{
    [self.activityIndicator startAnimating];
}

- (void)waveformViewDidRender:(FDWaveformView *)waveformView
{
    _waveformImageView.hidden = NO;
    [self.activityIndicator stopAnimating];
}


@end
