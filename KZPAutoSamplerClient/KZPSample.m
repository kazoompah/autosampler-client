//
//  KZPSample.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 26/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPSample.h"
#import "KZPSampleFileManager.h"
#import "KZPAudioEngine.h"
#import "KZPAudioProcessing.h"

@interface KZPSample ()

@property (strong, nonatomic) AVAudioFile *audioFile;
@property (strong, nonatomic) AVAudioPCMBuffer *audioPCMBuffer;

@property (nonatomic) BOOL playbackWasRestarted;

@end

@implementation KZPSample

- (instancetype)initWithFileURL:(NSURL *)fileURL
{
    if (!fileURL) return nil;
    
    self = [super init];
    if (self) {
        _audioFileURL = fileURL;
        [[KZPSampleFileManager sharedFileManager] sampleOwner:self refersToSampleAtFileURL:_audioFileURL];
    }
    return self;
}

+ (KZPSample *)sampleWithFileURL:(NSURL *)audioFileURL
{
    return [[KZPSample alloc] initWithFileURL:audioFileURL];
}

+ (KZPSample *)sampleWithSample:(KZPSample *)sample
{
    return [KZPSample sampleWithFileURL:sample.audioFileURL];
}

- (instancetype)initWithSampleID:(NSString *)sampleID
{
    NSURL *fileURL; // generate from sampleID
    return [self initWithFileURL:fileURL];
}

- (void)setShouldPlayInReverse:(BOOL)shouldPlayInReverse
{
    if (shouldPlayInReverse != _shouldPlayInReverse && _audioPCMBuffer.frameLength > 0) {
        [KZPAudioProcessing reverseSamples:_audioPCMBuffer.audioBufferList->mBuffers[0].mData
                                  nSamples:_audioPCMBuffer.frameLength];
    }
    _shouldPlayInReverse = shouldPlayInReverse;
}

- (void)loadWithCompletion:(void (^)(void))completionBlock failure:(void (^)(void))failureBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError *error;
        _audioFile = [[AVAudioFile alloc] initForReading:_audioFileURL commonFormat:AVAudioPCMFormatFloat32 interleaved:NO error:&error];
        
        if (error) {
            NSLog(@"Error: could not load %@: %@", _audioFileURL, [error localizedDescription]);
            dispatch_async(dispatch_get_main_queue(), ^{
                failureBlock();
            });
            return;
        }
        
        _audioPCMBuffer = [[AVAudioPCMBuffer alloc] initWithPCMFormat:_audioFile.processingFormat frameCapacity:(AVAudioFrameCount)_audioFile.length];
        
        if (![self loadBuffer:_audioPCMBuffer fromAudioFile:_audioFile]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                failureBlock();
            });
            return;
        }
        
        [self preprocessSampleBuffer];
        
        _playerNode = [[AVAudioPlayerNode alloc] init];
        _loaded = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock();
        });
    });
}

- (AVAudioFormat *)format
{
    return _audioPCMBuffer.format;
}

- (BOOL)loadBuffer:(AVAudioPCMBuffer *)audioPCMBuffer fromAudioFile:(AVAudioFile *)audioFile
{
    NSError *error;
    NSAssert(audioPCMBuffer.format.channelCount == audioFile.fileFormat.channelCount, @"Oops!");
    if (![audioFile readIntoBuffer:audioPCMBuffer error:&error]) {
        NSLog(@"Error: could not read into buffer: %@", [error localizedDescription]);
        return NO;
    }

    return YES;
}

- (void)restartPlaybackIgnoreLoop:(BOOL)ignoreLoop
{
    if (!_loaded) return;
    if (_playing) _playbackWasRestarted = YES;
    _playing = YES;
    
    AVAudioPlayerNodeBufferOptions playerOptions = AVAudioPlayerNodeBufferInterrupts;
    if (_shouldLoop && !ignoreLoop) playerOptions |= AVAudioPlayerNodeBufferLoops;
    
    [_playerNode scheduleBuffer:_audioPCMBuffer atTime:nil options:playerOptions completionHandler:^{
        if (_playbackWasRestarted) {
            _playbackWasRestarted = NO;
        } else {
            _playing = NO;
            [_delegate sampleDidFinishPlaying];
        }
    }];
    [_playerNode play];
    [_delegate sampleDidStartPlaying];
}

- (void)stopPlayback
{
    _playbackWasRestarted = NO;
    [_playerNode stop];
}

- (void)dealloc
{
    [[KZPAudioEngine sharedEngine] detachSample:self];
    if (_audioFileURL) {
        [[KZPSampleFileManager sharedFileManager] sampleOwner:self attemptToRemoveSampleAtFileURL:_audioFileURL];
    }
}

- (void)preprocessSampleBuffer
{
    Float32 *samples = _audioPCMBuffer.audioBufferList->mBuffers[0].mData;
    uint32_t length = _audioPCMBuffer.frameLength;
    
    if (_audioPCMBuffer.frameLength > 0) {
        [KZPAudioProcessing correctBiasForSamples:samples nSamples:length];
        [KZPAudioProcessing normalizeSamples:samples nSamples:length];
        
        // generate the reverse buffer rather than in-place, so we can chop the quiet parts of the release tail off the front and resize the buffer
    }
}


@end
