//
//  KZPFileUploader.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 2/11/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KZPFileUploaderDelegate <NSObject>

- (void)fileUploadComplete;
- (void)fileUploadFailed;
- (void)fileUploadProgress:(double)progress;

@end

// TODO: extract subclass 'KZPFileTransmitter' ?

@interface KZPFileUploader : NSObject

@property (weak, nonatomic) id<KZPFileUploaderDelegate> delegate;

@property (strong, nonatomic) NSString *filename;
@property (strong, nonatomic) NSString *remotePath;

@property (strong, nonatomic, readonly) NSString *localFilePath;
@property (strong, nonatomic, readonly) NSURL *localFileURL;

- (void)upload;

@end
