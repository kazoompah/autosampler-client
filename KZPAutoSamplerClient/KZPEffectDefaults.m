//
//  KZPEffectDefaults.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 6/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPEffectDefaults.h"

static KZPEffectDefaults *sharedInstance;

@interface KZPEffectDefaults ()

@property (strong, nonatomic) NSDictionary *effectsDefaultsDictionary;

@end

@implementation KZPEffectDefaults

+ (KZPEffectDefaults *)sharedDefaults
{
    if (!sharedInstance) sharedInstance = [[KZPEffectDefaults alloc] init];
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSString *effectDefaultsFilePath = [[NSBundle mainBundle] pathForResource:@"KZPEffectDefaults" ofType:@"plist"];
        _effectsDefaultsDictionary = [NSDictionary dictionaryWithContentsOfFile:effectDefaultsFilePath];
    }
    return self;
}

- (float)defaultValueForEffect:(NSString *)effectID
{
    return [_effectsDefaultsDictionary[effectID] floatValue];
}

@end
