//
//  KZPAudioEffect+Creator.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 6/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPAudioEffect+Creator.h"
#import "KZPReverbEffect.h"
#import "KZPPitchShiftEffect.h"
#import "KZPTimeStretchEffect.h"
#import "KZPTapeSpeedEffect.h"
#import "KZPBassifyEffect.h"
#import "KZPRingModEffect.h"
#import "KZPBitCrusherEffect.h"
#import "KZPDelayEffect.h"
#import "KZPLimiter.h"
#import "KZPDistortionEffect.h"
#import "KZPEffectDefaults.h"

static NSDictionary *effectClassByID;

@implementation KZPAudioEffect (Creator)

+ (KZPAudioEffect *)effectWithEffectID:(NSString *)effectID
{
    if (!effectClassByID) {
        [self generateClassDictionary];
    }
    
    Class effectClass = effectClassByID[effectID];
    KZPAudioEffect *effect = [[effectClass alloc] init];
    [effect applyContinuousValue:[[KZPEffectDefaults sharedDefaults] defaultValueForEffect:effectID]];
    return effect;
}

+ (void)generateClassDictionary
{
    effectClassByID = @{@"Reverb": [KZPReverbEffect class],
                        @"Delay": [KZPDelayEffect class],
                        @"Pitch": [KZPPitchShiftEffect class],
                        @"Stretch": [KZPTimeStretchEffect class],
                        @"Speed": [KZPTapeSpeedEffect class],
                        @"Bassify": [KZPBassifyEffect class],
                        @"RingMod": [KZPRingModEffect class],
                        @"Crush": [KZPBitCrusherEffect class],
                        @"Limiter": [KZPLimiter class],
                        @"Distortion": [KZPDistortionEffect class]};
}

@end
