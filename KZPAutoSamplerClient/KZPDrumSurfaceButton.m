//
//  KZPDrumPadButton.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 20/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPDrumSurfaceButton.h"
#import "KZPInteractionState.h"
#import "KZPAudioEngine.h"
#import "KZPQueueItem.h"
#import "KZPPerformancePrefsViewController.h"
#import "KZPEffectsViewController.h"
#import "KZPPersistentStore.h"

@interface KZPDrumSurfaceButton () <KZPDrumSurfaceCellDelegate, KZPInteractionStateObserver, KZPPerformancePrefsDelegate, KZPEffectsChainDelegate, KZPEffectsViewControllerDelegate>

@property (nonatomic) BOOL currentlyLoading;
@property (nonatomic) CGPoint drumSurfaceLocation;
@property (nonatomic) BOOL currentlyEditing;
@property (strong, nonatomic) KZPPerformancePrefsViewController *performancePrefsViewController;
@property (strong, nonatomic) KZPEffectsViewController *effectsViewController;
@property (strong, nonatomic) NSString *buttonLabel;

@end

@implementation KZPDrumSurfaceButton

+ (NSString *)drumSurfaceLocationKeyWithLocation:(CGPoint)drumSurfaceLocation
{
    return [NSString stringWithFormat:@"%d'%d", (int)drumSurfaceLocation.x, (int)drumSurfaceLocation.y];
}

- (instancetype)initWithDrumSurfaceLocation:(CGPoint)drumSurfaceLocation
                        savedDrumButtonInfo:(NSDictionary *)savedDrumButtonInfo
{
    self = [super init];
    if (self) {
        _drumSurfaceLocation = drumSurfaceLocation;
        _performanceType = KZPDrumSurfacePerformanceTypeNormal;
        _effectsChain = [[KZPEffectsChain alloc] init];
        _effectsChain.delegate = self;
        [self restoreState];
        [[KZPInteractionState sharedInteractionState] registerInteractionObserver:self];
    }
    return self;
}

- (void)restoreState
{
    _currentlyLoading = YES;
    NSUInteger channel = [self channelNumber];
    NSURL *sampleURL = [[KZPPersistentStore sharedStore] sampleURLForButtonID:channel];
    NSDictionary *effectsChainInfo = [[KZPPersistentStore sharedStore] effectsChainInfoForButtonID:channel];
    NSDictionary *performanceInfo = [[KZPPersistentStore sharedStore] performanceInfoForButtonID:channel];
    _buttonLabel = [[KZPPersistentStore sharedStore] labelForButtonID:channel];
    
    [self copySample:[KZPSample sampleWithFileURL:sampleURL] withCompletion:^{
        [_effectsChain loadWithDictionary:effectsChainInfo withCompletion:^{
            [_drumSurfaceButtonView showEffectsIndicator:[[_effectsChain activeEffects] count] > 0];
            [self loadPerformanceSettingsFromDictionary:performanceInfo];
            _currentlyLoading = NO;
            [self returnToStandbyState];
        }];
    }];
}

- (NSString *)label
{
    NSString *labelWithSpaces = [_buttonLabel length] > 0 ? _buttonLabel : [[self class] drumSurfaceLocationKeyWithLocation:_drumSurfaceLocation];
    return [labelWithSpaces stringByReplacingOccurrencesOfString:@" " withString:@"_"];
}

- (void)setDrumSurfaceButtonView:(KZPDrumSurfaceCollectionViewCell *)drumSurfaceButtonView
{
    _drumSurfaceButtonView = drumSurfaceButtonView;
    _drumSurfaceButtonView.delegate = self;
    _drumSurfaceButtonView.padLabel.text = _buttonLabel;
    [_drumSurfaceButtonView showEffectsIndicator:[[_effectsChain activeEffects] count] > 0];
    if (_sample) [_drumSurfaceButtonView displayWaveformForAudioFileURL:_sample.audioFileURL];
    
    // TODO: duplication
    [_drumSurfaceButtonView updatePerformanceIndicatorsWithLoop:[_sample shouldLoop]
                                                           hold:_performanceType == KZPDrumSurfacePerformanceTypeHold
                                                      exclusive:_playsExclusive];
    [self returnToStandbyState];
}

- (void)loadPerformanceSettingsFromDictionary:(NSDictionary *)performanceInfo
{
    if (!performanceInfo) return;
    
    _performanceType = (KZPDrumSurfacePerformanceType)[performanceInfo[kPerformanceTouchMode] integerValue];
    _playsExclusive = [performanceInfo[kPerformanceExclusive] boolValue];
    
    // TODO: assumes sample exists - check this in other places. If the sample does not exist then
    // these settings won't stick anywhere
    _sample.shouldLoop = [performanceInfo[kPerformanceLooping] boolValue];
    _sample.shouldPlayInReverse = [performanceInfo[kPerformanceReverse] boolValue];
    
    // TODO: duplication
    [_drumSurfaceButtonView updatePerformanceIndicatorsWithLoop:[_sample shouldLoop]
                                                           hold:_performanceType == KZPDrumSurfacePerformanceTypeHold
                                                      exclusive:_playsExclusive];
}

- (void)setButtonLabel:(NSString *)buttonLabel
{
    if ([buttonLabel isEqualToString:@""]) buttonLabel = nil;
    _drumSurfaceButtonView.padLabel.text = buttonLabel;
    [[KZPPersistentStore sharedStore] saveLabel:buttonLabel forButtonID:[self channelNumber]];
    _buttonLabel = buttonLabel;
}


#pragma mark - <KZPDrumSurfaceCellDelegate>


- (void)drumSurfaceButtonEngaged
{
    KZPDrumSurfaceUsageMode drumSurfaceUsageMode = [[KZPInteractionState sharedInteractionState] drumSurfaceUsageMode];
    
    switch (drumSurfaceUsageMode) {
        case KZPDrumSurfaceUsageModeDisabled: break;
        case KZPDrumSurfaceUsageModePerformance: [self handlePerformanceTrigger]; break;
        
        default: break;
    }
}

- (void)drumSurfaceButtonReleased
{
    KZPDrumSurfaceUsageMode drumSurfaceUsageMode = [[KZPInteractionState sharedInteractionState] drumSurfaceUsageMode];
    
    switch (drumSurfaceUsageMode) {
        case KZPDrumSurfaceUsageModeDisabled: break;
        case KZPDrumSurfaceUsageModePerformance: [self handlePerformanceRelease]; break;
        case KZPDrumSurfaceUsageModeClearSample: [self clearSample]; break;
        case KZPDrumSurfaceUsageModeQueueAssignment: [self copyCurrentQueueSample]; break;
        case KZPDrumSurfaceUsageModeCopySample: [self handleDrumPadSampleCopy]; break;
        case KZPDrumSurfaceUsageModeEditLabel: [self editLabel]; break;
        case KZPDrumSurfaceUsageModePerformancePrefs: [self editPerformancePrefs]; break;
        case KZPDrumSurfaceUsageModeApplyEffects: [self editEffects]; break;
        case KZPDrumSurfaceUsageModeCopyEffects: [self handleDrumPadEffectsCopy]; break;
            
        default: NSLog(@"Drum surface usage mode not handled"); break;
    }
}

- (void)editLabel
{
    UIAlertController *editLabelAlert = [UIAlertController alertControllerWithTitle:@"Edit label" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [editLabelAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = _buttonLabel ? _buttonLabel : @"New button label";
        [textField becomeFirstResponder];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self returnToStandbyState];
    }];
    UIAlertAction *acceptAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _buttonLabel = editLabelAlert.textFields[0].text;
        _drumSurfaceButtonView.padLabel.text = _buttonLabel;
        [[KZPPersistentStore sharedStore] saveLabel:_buttonLabel forButtonID:[self channelNumber]];
        [self returnToStandbyState];
    }];
    
    [editLabelAlert addAction:cancelAction];
    [editLabelAlert addAction:acceptAction];
    
    [_delegate presentViewController:editLabelAlert];
    [_drumSurfaceButtonView displayState:KZPDrumSurfaceButtonStateEditing];
    _currentlyEditing = YES;
}

- (void)returnToStandbyState
{
    if (_currentlyLoading) {
        [_drumSurfaceButtonView displayState:KZPDrumSurfaceButtonStateLoading];
    } else {
        [_drumSurfaceButtonView displayState:_sample ? KZPDrumSurfaceButtonStateActive : KZPDrumSurfaceButtonStateInactive];
    }
}

- (void)editEffects
{
    [_sample stopPlayback];
    [_drumSurfaceButtonView displayState:KZPDrumSurfaceButtonStateEditing];
    _effectsViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"effectsViewController"];
    _effectsViewController.preferredContentSize = CGSizeMake(500, 320);
    _effectsViewController.modalPresentationStyle = UIModalPresentationPopover;
    _effectsViewController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    _effectsViewController.popoverPresentationController.sourceRect = _drumSurfaceButtonView.frame;
    [_delegate presentViewController:_effectsViewController];
    _effectsViewController.effectsChain = _effectsChain;
    _effectsViewController.delegate = self;
    _currentlyEditing = YES;
}

- (void)editPerformancePrefs
{
    [_sample stopPlayback];
    [_drumSurfaceButtonView displayState:KZPDrumSurfaceButtonStateEditing];
    _performancePrefsViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"performancePrefsViewController"];
    _performancePrefsViewController.preferredContentSize = CGSizeMake(280, 240);
    _performancePrefsViewController.modalPresentationStyle = UIModalPresentationPopover;
    _performancePrefsViewController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown;
    _performancePrefsViewController.popoverPresentationController.sourceRect = _drumSurfaceButtonView.frame;
    [_delegate presentViewController:_performancePrefsViewController];
    _performancePrefsViewController.delegate = self;
    _performancePrefsViewController.loopSwitch.on = _sample.shouldLoop;
    _performancePrefsViewController.reverseSwitch.on = _sample.shouldPlayInReverse;
    _performancePrefsViewController.exclusiveSwitch.on = _playsExclusive;
    _performancePrefsViewController.playPrefSegmentedControl.selectedSegmentIndex = _performanceType;
    [_performancePrefsViewController refresh];
    _currentlyEditing = YES;
}

- (void)handlePerformanceTrigger
{
    switch (_performanceType) {
        case KZPDrumSurfacePerformanceTypeNormal: [_sample restartPlaybackIgnoreLoop:NO]; break;
        case KZPDrumSurfacePerformanceTypeHold: [_sample restartPlaybackIgnoreLoop:NO]; break;
        case KZPDrumSurfacePerformanceTypeToggle: [_sample isPlaying] ? [_sample stopPlayback] : [_sample restartPlaybackIgnoreLoop:NO]; break;
        default: break;
    }
}

- (void)handlePerformanceRelease
{
    if (_performanceType == KZPDrumSurfacePerformanceTypeHold) {
        [_sample stopPlayback];
    }
}

// TODO: sometimes causes a problem with the playback engine hanging or not running a completion block 
- (void)clearSample
{
    [_sample stopPlayback];
    [[KZPAudioEngine sharedEngine] detachSample:_sample];
    _sample = nil;
    [_drumSurfaceButtonView displayWaveformForAudioFileURL:nil];
    [[KZPPersistentStore sharedStore] saveSampleURL:nil forButtonID:[self channelNumber]];
    [self returnToStandbyState];
}

- (void)copyCurrentQueueSample
{
    [_sample stopPlayback];
    [[KZPAudioEngine sharedEngine] detachSample:_sample];
    _sample = nil;
    KZPQueueItem *queueItem = [[KZPInteractionState sharedInteractionState] fetchTemporaryObjectOfType:[KZPQueueItem class]];
    if (queueItem) {
        [self copySample:queueItem.sample withCompletion:^{
            [queueItem requestDeletion];
            [[KZPInteractionState sharedInteractionState] queueSampleSelectionEnded];
            [[KZPPersistentStore sharedStore] saveSampleURL:_sample.audioFileURL forButtonID:[self channelNumber]];
        }];
    }
}

// TODO: class is getting too complicated - use categories?
- (void)handleDrumPadSampleCopy
{
    if (_currentlyEditing) {
        _currentlyEditing = NO;
        [[KZPInteractionState sharedInteractionState] removeTemporaryObject];
        [self returnToStandbyState];
    } else {
        KZPSample *sourceSample = [[KZPInteractionState sharedInteractionState] fetchTemporaryObjectOfType:[KZPSample class]];
        if (sourceSample) {
            [self copySample:sourceSample withCompletion:^{
                [[KZPPersistentStore sharedStore] saveSampleURL:_sample.audioFileURL forButtonID:[self channelNumber]];
            }];
        } else {
            if (_sample) {
                _currentlyEditing = YES;
                [[KZPInteractionState sharedInteractionState] provideTemporaryObject:_sample];
                [_drumSurfaceButtonView displayState:KZPDrumSurfaceButtonStateEditing];
            }
        }
    }
}

- (void)copySample:(KZPSample *)sample withCompletion:(void (^)(void))completionBlock
{
    if (!sample) { completionBlock(); return; }
    
    [_drumSurfaceButtonView displayState:KZPDrumSurfaceButtonStateLoading];
    _sample = [KZPSample sampleWithSample:sample];
    _sample.delegate = self;
    [_sample loadWithCompletion:^{
        _effectsChain.format = _sample.format;
        [[KZPAudioEngine sharedEngine] activateSample:_sample
                                            onChannel:[self channelNumber]
                                     withEffectsChain:_effectsChain];
        [self returnToStandbyState];
        [_drumSurfaceButtonView displayWaveformForAudioFileURL:_sample.audioFileURL];
        completionBlock();
    } failure:^{
        [_drumSurfaceButtonView displayState:KZPDrumSurfaceButtonStateError];
        _sample = nil;
        [[KZPPersistentStore sharedStore] saveSampleURL:nil forButtonID:[self channelNumber]];
        completionBlock();
    }];
}

- (void)handleDrumPadEffectsCopy
{
    if (_currentlyEditing) {
        _currentlyEditing = NO;
        [[KZPInteractionState sharedInteractionState] removeTemporaryObject];
        [self returnToStandbyState];
    } else {
        KZPEffectsChain *effectsChain = [[KZPInteractionState sharedInteractionState] fetchTemporaryObjectOfType:[KZPEffectsChain class]];
        if (effectsChain) {
            [_effectsChain copyEffectsChain:effectsChain withCompletion:^{
                [self effectsSettingsDidChange];
            }];
        } else {
            _currentlyEditing = YES;
            [[KZPInteractionState sharedInteractionState] provideTemporaryObject:_effectsChain];
            [_drumSurfaceButtonView displayState:KZPDrumSurfaceButtonStateEditing];
        }
    }
}

/* 
   Due to the drum surface being resizeable, the channels associated with each drum
   pad button are calculated without relying on grid dimensions. The advantage of this
   is that virtually no house-keeping needs to happen after a resize event.
   1 2 5 10 ...
   3 4 7 ......
   6 8 9 ......
   11 ......... etc
*/
- (AVAudioNodeBus)channelNumber
{
    int x = (int)self.drumSurfaceLocation.x + 1, y = (int)self.drumSurfaceLocation.y + 1;
    int subdimension = MAX(x, y);
    return (AVAudioNodeBus)(subdimension * subdimension - 2 * abs(x - y) + (y > x ? 1 : 0));
}

- (void)reset
{
    [self clearSample];
    _effectsChain = [[KZPEffectsChain alloc] init];
    _effectsChain.delegate = self;
    [self effectsSettingsDidChange];
}


#pragma mark - <KZPSampleDelegate>


- (void)sampleDidStartPlaying
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_drumSurfaceButtonView displayState:KZPDrumSurfaceButtonStateHighlighted];
        [_delegate drumSurfaceButtonWasTriggered:self];
    });
}

- (void)sampleDidFinishPlaying
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self returnToStandbyState];
    });
}


#pragma mark - <KZPInteractionStateObserver>


- (void)interactionStateDidChange
{
    if (_currentlyEditing) {
        _currentlyEditing = NO;
        [self returnToStandbyState];
    }
}


#pragma mark - <KZPPerformancePrefsDelegate>


// TODO: send back the controller object instead of storing it
- (void)performancePrefsDidChange
{
    _sample.shouldLoop = [_performancePrefsViewController.loopSwitch isOn];
    _sample.shouldPlayInReverse = [_performancePrefsViewController.reverseSwitch isOn];
    _playsExclusive = [_performancePrefsViewController.exclusiveSwitch isOn];
    _performanceType = (KZPDrumSurfacePerformanceType)[_performancePrefsViewController.playPrefSegmentedControl selectedSegmentIndex];
    
    [[KZPPersistentStore sharedStore] savePerformanceInfo:@{kPerformanceTouchMode: @(_performanceType),
                                                            kPerformanceLooping: @(_sample.shouldLoop),
                                                            kPerformanceExclusive: @(_playsExclusive),
                                                            kPerformanceReverse: @(_sample.shouldPlayInReverse)}
                                               forButtonID:[self channelNumber]];
    
    [_drumSurfaceButtonView updatePerformanceIndicatorsWithLoop:[_sample shouldLoop]
                                                           hold:_performanceType == KZPDrumSurfacePerformanceTypeHold
                                                      exclusive:_playsExclusive];
}


#pragma mark - <KZPEffectsChainDelegate>


- (void)effectsChainNeedsReconnecting
{
    if (_sample) {
        [[KZPAudioEngine sharedEngine] activateSample:_sample onChannel:[self channelNumber] withEffectsChain:_effectsChain];
    }
}


#pragma mark - <KZPEffectsViewControllerDelegate>


- (void)effectsSettingsDidChange
{
    [[KZPPersistentStore sharedStore] saveEffectsInfo:[_effectsChain currentSettings] forButtonID:[self channelNumber]];
    [_drumSurfaceButtonView showEffectsIndicator:[[_effectsChain activeEffects] count] > 0];
    [_sample restartPlaybackIgnoreLoop:YES];
}

- (void)dealloc
{
    [[KZPInteractionState sharedInteractionState] unregisterInteractionObserver:self];
}

@end
