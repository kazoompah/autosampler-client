//
//  KZPPeristentStore.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 7/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kUserData               @"user_data"
#define kCurrentBank            @"current_bank"
#define kBanks                  @"banks"
#define kBankDimensions         @"dimensions"
#define kBankDimensionsX        @"x"
#define kBankDimensionsY        @"y"
#define kDrumSurfaceButtons     @"buttons"
#define kBankName               @"bank_name"
#define kButtonLabel            @"label"
#define kButtonSampleFilename   @"sample"
#define kButtonPerformanceInfo  @"performance"
#define kButtonEffectsInfo      @"effects"
#define kPerformanceTouchMode   @"touch_mode"
#define kPerformanceLooping     @"looping"
#define kPerformanceExclusive   @"exclusive"
#define kPerformanceReverse     @"reverse"

#define DEFAULT_BANK_SIZE_X     5
#define DEFAULT_BANK_SIZE_Y     4

@import UIKit;

@interface KZPPersistentStore : NSObject

+ (KZPPersistentStore *)sharedStore;

@property (nonatomic) NSUInteger bankNumber;

- (NSArray *)bankList;
- (void)addNewBank;

- (CGSize)bankDimensions;
- (void)saveBankDimensions:(CGSize)bankDimensions;

- (NSURL *)sampleURLForButtonID:(NSUInteger)buttonID;
- (NSDictionary *)effectsChainInfoForButtonID:(NSUInteger)buttonID;
- (NSDictionary *)performanceInfoForButtonID:(NSUInteger)buttonID;
- (NSString *)labelForButtonID:(NSUInteger)buttonID;

- (void)saveSampleURL:(NSURL *)sampleURL forButtonID:(NSUInteger)buttonID;
- (void)savePerformanceInfo:(NSDictionary *)performanceInfo forButtonID:(NSUInteger)buttonID;
- (void)saveEffectsInfo:(NSDictionary *)effectsInfo forButtonID:(NSUInteger)buttonID;
- (void)saveLabel:(NSString *)label forButtonID:(NSUInteger)buttonID;

@end
