//
//  KZPFunctionsViewController.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 18/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPFunctionsCollectionViewController.h"
#import "KZPInteractionState.h"
#import "KZPFunctionCollectionViewCell.h"

@interface KZPFunctionsCollectionViewController () <KZPInteractionStateObserver>

@property (strong, nonatomic) NSArray *functionIDs;
@property (strong, nonatomic) NSMutableArray *functionCells;

@end

@implementation KZPFunctionsCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.layer.shadowRadius = 4;
    self.view.layer.shadowOpacity = 0.3;
    self.view.layer.masksToBounds = NO;
    self.view.layer.shadowOffset = CGSizeMake(0, -3);
    
    _functionIDs = @[@"Clear", @"Prefs", @"Effects", @"Edit", @"CopySample", @"CopyEffects", @"Export", @"Reset"];
    _functionCells = [NSMutableArray array];
    [[KZPInteractionState sharedInteractionState] registerInteractionObserver:self];
}


#pragma mark - <KZPInteractionStateObserver>


- (void)interactionStateDidChange
{
    for (KZPFunctionCollectionViewCell *cell in _functionCells) {
        cell.functionButton.selected = NO;
    }
}


#pragma mark - <UICollectionViewDataSource>


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.functionIDs count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *functionName = _functionIDs[indexPath.row];
    NSString *reuseIdentifier = [NSString stringWithFormat:@"functionCell%@", functionName];
    
    KZPFunctionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    [_functionCells addObject:cell];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)collectionViewLayout;
    NSUInteger numberOfFunctions = [self.functionIDs count];
    
    CGSize functionsPanelSize = self.view.frame.size;
    CGFloat functionsPanelSizeInsetWidth = functionsPanelSize.width - flowLayout.sectionInset.left - flowLayout.sectionInset.right;
    CGFloat functionsPanelSizeInsetHeight = functionsPanelSize.height - flowLayout.sectionInset.top - flowLayout.sectionInset.bottom;
    CGFloat functionsPanelSizeTotalButtonWidth = functionsPanelSizeInsetWidth - (numberOfFunctions - 1) * flowLayout.minimumInteritemSpacing;
    CGFloat buttonDimension = functionsPanelSizeTotalButtonWidth / numberOfFunctions;
    if (buttonDimension > functionsPanelSizeInsetHeight) {
        buttonDimension = functionsPanelSizeInsetHeight;
    }
    return CGSizeMake(buttonDimension, buttonDimension);
}

- (void)functionButtonPressed:(UIButton *)functionButton forDrumUsageMode:(KZPDrumSurfaceUsageMode)drumUsageMode
{
    if (functionButton.selected) {
        [[KZPInteractionState sharedInteractionState] functionDeselected];
    } else {
        [[KZPInteractionState sharedInteractionState] functionSelectedWithDrumSurfaceUsageMode:drumUsageMode];
        functionButton.selected = YES;
    }
}


#pragma mark - Actions


- (IBAction)copyFunctionButtonPressed:(UIButton *)sender
{
    [self functionButtonPressed:sender forDrumUsageMode:KZPDrumSurfaceUsageModeCopySample];
}

- (IBAction)clearFunctionButtonPressed:(UIButton *)sender
{
    [self functionButtonPressed:sender forDrumUsageMode:KZPDrumSurfaceUsageModeClearSample];
}

- (IBAction)copyEffectsFunctionButtonPressed:(UIButton *)sender
{
    [self functionButtonPressed:sender forDrumUsageMode:KZPDrumSurfaceUsageModeCopyEffects];
}

- (IBAction)playPrefsButtonPressed:(UIButton *)sender
{
    [self functionButtonPressed:sender forDrumUsageMode:KZPDrumSurfaceUsageModePerformancePrefs];
}

- (IBAction)editFunctionButtonPressed:(UIButton *)sender
{
    [self functionButtonPressed:sender forDrumUsageMode:KZPDrumSurfaceUsageModeEditLabel];
}

- (IBAction)effectsFunctionButtonPressed:(UIButton *)sender
{
    [self functionButtonPressed:sender forDrumUsageMode:KZPDrumSurfaceUsageModeApplyEffects];
}

- (IBAction)exportFunctionButtonPressed:(UIButton *)sender {
    [self.delegate exportFunctionSelected];
}

- (IBAction)resetFunctionButtonPressed:(UIButton *)sender {
    [self.delegate resetFunctionSelected];
}

- (IBAction)helpFunctionButtonPressed:(UIButton *)sender
{
    // This button is different
}

@end
