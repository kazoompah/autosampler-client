//
//  KZPInteractionState.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 20/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    KZPDrumSurfaceUsageModeDisabled,
    KZPDrumSurfaceUsageModePerformance,
    KZPDrumSurfaceUsageModeQueueAssignment,
    KZPDrumSurfaceUsageModeCopySample,
    KZPDrumSurfaceUsageModeCopyEffects,
    KZPDrumSurfaceUsageModeClearSample,
    KZPDrumSurfaceUsageModeEditLabel,
    KZPDrumSurfaceUsageModePerformancePrefs,
    KZPDrumSurfaceUsageModeApplyEffects
} KZPDrumSurfaceUsageMode;

typedef enum {
    KZPAppUsageModePerformance,
    KZPAppUsageModeSampleQueue,
    KZPAppUsageModeFunctions
} KZPAppUsageMode;

@protocol KZPInteractionStateObserver <NSObject>

- (void)interactionStateDidChange;

@end

/*
    Provide important global state information to callers trying to
    determine context-specific course-of-action
 
 */

@interface KZPInteractionState : NSObject

+ (KZPInteractionState *)sharedInteractionState;

@property (nonatomic, readonly) KZPAppUsageMode appUsageMode;
@property (nonatomic, readonly) KZPDrumSurfaceUsageMode drumSurfaceUsageMode;

- (void)registerInteractionObserver:(id<KZPInteractionStateObserver>)observer;
- (void)unregisterInteractionObserver:(id<KZPInteractionStateObserver>)observer;

- (void)provideTemporaryObject:(id)temporaryObject;
- (void)removeTemporaryObject;
- (id)fetchTemporaryObjectOfType:(Class)expectedType;

- (void)drumSurfaceDidLoad;
- (void)queueSampleWasSelected;
- (void)queueSampleSelectionEnded;

- (void)functionSelectedWithDrumSurfaceUsageMode:(KZPDrumSurfaceUsageMode)drumSurfaceUsageMode;
- (void)functionDeselected;

@end
