//
//  KZPPerformancePrefsViewController.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 3/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

@import UIKit;

typedef enum {
    KZPDrumSurfacePerformanceTypeNormal = 0,    // plays from beginning to end
    KZPDrumSurfacePerformanceTypeHold,          // starts playing when engaged, stops playing when released
    KZPDrumSurfacePerformanceTypeToggle         // starts playing when tapped, stops playing when tapped again
} KZPDrumSurfacePerformanceType;

@protocol KZPPerformancePrefsDelegate <NSObject>

- (void)performancePrefsDidChange;

@end

@interface KZPPerformancePrefsViewController : UIViewController

@property (weak, nonatomic) id<KZPPerformancePrefsDelegate> delegate;

@property (weak, nonatomic) IBOutlet UISwitch *loopSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *exclusiveSwitch;
@property (weak, nonatomic) IBOutlet UISegmentedControl *playPrefSegmentedControl;
@property (weak, nonatomic) IBOutlet UISwitch *reverseSwitch;

- (void)refresh;

@end
