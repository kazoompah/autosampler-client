//
//  KZPPeristentStore.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 7/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPPersistentStore.h"
#import "KZPSampleFileManager.h"

static KZPPersistentStore *sharedInstance;

@interface KZPPersistentStore ()

@property (strong, nonatomic) NSMutableDictionary *userData;
@property (strong, nonatomic) NSMutableDictionary *bankData;

@end

@implementation KZPPersistentStore

+ (KZPPersistentStore *)sharedStore
{
    if (!sharedInstance) sharedInstance = [[KZPPersistentStore alloc] init];
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] valueForKey:kUserData];
        if (!userData) {
            [self initializeUserData];
        } else {
            _userData = CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFPropertyListRef)userData, kCFPropertyListMutableContainers));
        }
        _bankNumber = [_userData[kCurrentBank] unsignedIntegerValue];
        _bankData = _userData[kBanks][_bankNumber];
    }
    return self;
}

- (void)initializeUserData
{
    _userData = _userData = [NSMutableDictionary dictionary];
    _userData[kBanks] = [NSMutableArray array];
    [self addNewBank];
}

- (NSMutableDictionary *)createBank
{
    NSMutableDictionary *bank = [NSMutableDictionary dictionary];
    bank[kDrumSurfaceButtons] = [NSMutableDictionary dictionary];
    bank[kBankName] = @"Untitled";
    bank[kBankDimensions] = @{kBankDimensionsX: @(DEFAULT_BANK_SIZE_X),
                              kBankDimensionsY: @(DEFAULT_BANK_SIZE_Y)};
    return bank;
}

- (NSArray *)bankList
{
    NSMutableArray *bankNames = [NSMutableArray array];
    for (NSMutableDictionary *bank in _userData[kBanks]) {
        [bankNames addObject:bank[kBankName]];
    }
    return bankNames;
}

- (void)setBankNumber:(NSUInteger)bankNumber
{
    NSMutableArray *banks = _userData[kBanks];
    if (bankNumber < [banks count]) {
        _bankData = _userData[kBanks][bankNumber];
        _userData[kCurrentBank] = @(bankNumber);
        [self saveUserData];
        _bankNumber = bankNumber;
    }
}

- (void)addNewBank
{
    [_userData[kBanks] addObject:[self createBank]];
    [self setBankNumber:[_userData[kBanks] count] - 1];
}

- (CGSize)bankDimensions
{
    NSDictionary *dimensions = _bankData[kBankDimensions];
    return CGSizeMake([dimensions[kBankDimensionsX] intValue],
                      [dimensions[kBankDimensionsY] intValue]);
}

- (void)saveBankDimensions:(CGSize)bankDimensions
{
    _bankData[kBankDimensions] = @{kBankDimensionsX: @(bankDimensions.width),
                                   kBankDimensionsY: @(bankDimensions.height)};
    [self saveUserData];
}

- (NSURL *)sampleURLForButtonID:(NSUInteger)buttonID
{
    NSMutableDictionary *button = [self buttonInfoForButtonID:buttonID];
    NSString *fileName = button[kButtonSampleFilename];
    NSString *documents = [[KZPSampleFileManager sharedFileManager] documentsPath];
    return fileName ? [NSURL fileURLWithPath:[documents stringByAppendingPathComponent:fileName] isDirectory:NO] : nil;
}

- (NSDictionary *)effectsChainInfoForButtonID:(NSUInteger)buttonID
{
    NSMutableDictionary *button = [self buttonInfoForButtonID:buttonID];
    return button[kButtonEffectsInfo];
}

- (NSDictionary *)performanceInfoForButtonID:(NSUInteger)buttonID
{
    NSMutableDictionary *button = [self buttonInfoForButtonID:buttonID];
    return button[kButtonPerformanceInfo];
}

- (NSString *)labelForButtonID:(NSUInteger)buttonID
{
    NSMutableDictionary *button = [self buttonInfoForButtonID:buttonID];
    return button[kButtonLabel];
}

- (NSMutableDictionary *)buttonInfoForButtonID:(NSUInteger)buttonID
{
    NSString *buttonIDString = [NSString stringWithFormat:@"%lu", (unsigned long)buttonID];
    NSMutableDictionary *buttons = _bankData[kDrumSurfaceButtons];
    NSMutableDictionary *button = buttons[buttonIDString];
    if (!button) {
        return [self createButton:buttonIDString];
    }
    return button;
}

- (NSMutableDictionary *)createButton:(NSString *)buttonIDString
{
    NSMutableDictionary *button = [NSMutableDictionary dictionary];
    _bankData[kDrumSurfaceButtons][buttonIDString] = button;
    return button;
}

- (void)saveSampleURL:(NSURL *)sampleURL forButtonID:(NSUInteger)buttonID
{
    NSMutableDictionary *button = [self buttonInfoForButtonID:buttonID];
    if (sampleURL) {
        button[kButtonSampleFilename] = [sampleURL lastPathComponent];
    } else {
        [button removeObjectForKey:kButtonSampleFilename];
    }
    [self saveUserData];
}

- (void)savePerformanceInfo:(NSDictionary *)performanceInfo forButtonID:(NSUInteger)buttonID
{
    NSMutableDictionary *button = [self buttonInfoForButtonID:buttonID];
    if (performanceInfo) {
        button[kButtonPerformanceInfo] = performanceInfo;
    }
    [self saveUserData];
}

- (void)saveEffectsInfo:(NSDictionary *)effectsInfo forButtonID:(NSUInteger)buttonID
{
    NSMutableDictionary *button = [self buttonInfoForButtonID:buttonID];
    if (effectsInfo) {
        button[kButtonEffectsInfo] = effectsInfo;
    }
    [self saveUserData];
}

- (void)saveLabel:(NSString *)label forButtonID:(NSUInteger)buttonID
{
    NSMutableDictionary *button = [self buttonInfoForButtonID:buttonID];
    if (label) {
        button[kButtonLabel] = label;
    } else {
        [button removeObjectForKey:kButtonLabel];
    }
    [self saveUserData];
}

- (void)saveUserData
{
    [[NSUserDefaults standardUserDefaults] setValue:_userData forKey:kUserData];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
