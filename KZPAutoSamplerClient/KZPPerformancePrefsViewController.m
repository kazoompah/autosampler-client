//
//  KZPPerformancePrefsViewController.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 3/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPPerformancePrefsViewController.h"
#import "KZPInteractionState.h"

@interface KZPPerformancePrefsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *playPrefDescriptionLabel;

@end

@implementation KZPPerformancePrefsViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setPreferredContentSize:self.view.frame.size];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[KZPInteractionState sharedInteractionState] functionDeselected];
}

- (IBAction)playPrefSegmentDidChange:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == KZPDrumSurfacePerformanceTypeNormal && [_loopSwitch isOn]) {
        _loopSwitch.on = NO;
    }
    [self updateDescriptionLabel];
    [_delegate performancePrefsDidChange];
}

- (IBAction)loopSwitchChanged:(UISwitch *)sender
{
    if ([sender isOn] && _playPrefSegmentedControl.selectedSegmentIndex == KZPDrumSurfacePerformanceTypeNormal) {
        _playPrefSegmentedControl.selectedSegmentIndex = KZPDrumSurfacePerformanceTypeToggle;
        [self updateDescriptionLabel];
    }
    [_delegate performancePrefsDidChange];
}

- (IBAction)reverseSwitchChanged:(id)sender
{
    [_delegate performancePrefsDidChange];
}

- (IBAction)exclusiveSwitchChanged:(UISwitch *)sender
{
    [_delegate performancePrefsDidChange];
}

- (void)refresh
{
    [self updateDescriptionLabel];
}

- (void)updateDescriptionLabel
{
    KZPDrumSurfacePerformanceType performanceType = (KZPDrumSurfacePerformanceType)_playPrefSegmentedControl.selectedSegmentIndex;
    
    switch (performanceType) {
        case KZPDrumSurfacePerformanceTypeNormal: _playPrefDescriptionLabel.text = @"Sample plays from start to finish"; break;
        case KZPDrumSurfacePerformanceTypeHold: _playPrefDescriptionLabel.text = @"Sample plays while you hold your finger down"; break;
        case KZPDrumSurfacePerformanceTypeToggle: _playPrefDescriptionLabel.text = @"Touching the sample switches it on/off"; break;
        default: _playPrefDescriptionLabel.text = @"(Unknown performance type)"; break;
    }
}

@end
