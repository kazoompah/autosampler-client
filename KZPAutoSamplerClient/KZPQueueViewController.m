//
//  KZPQueueViewController.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 18/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPQueueViewController.h"
#import "KZPQueueItemTableViewCell.h"
#import "KZPInteractionState.h"
#import "KZPSampleReceiver.h"

@interface KZPQueueViewController () <KZPQueueItemDelegate, UITableViewDelegate, UITableViewDataSource, KZPInteractionStateObserver, KZPSampleReceiverDelegate>

@property (strong, nonatomic) KZPSampleReceiver *sampleReceiver;

@property (strong, nonatomic) NSMutableArray *pendingQueueSampleIDs; // Total list of queue samples from server
@property (strong, nonatomic) NSMutableArray *queueContents;        // Current active queue items
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *lockButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@end

@implementation KZPQueueViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _queueContents = [NSMutableArray array];
    
    _sampleReceiver = [[KZPSampleReceiver alloc] init];
    _sampleReceiver.delegate = self;
    
    self.view.layer.shadowRadius = 4;
    self.view.layer.shadowOpacity = 0.3;
    self.view.layer.masksToBounds = NO;
    self.view.layer.shadowOffset = CGSizeMake(3, 0);
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.6 alpha:1.0];
    [[KZPInteractionState sharedInteractionState] registerInteractionObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    self.view.backgroundColor = [UIColor colorWithRed:150.0/255 green:25.0/255 blue:25.0/255 alpha:1.0];
    
    KZPQueueItem *singleQueueItem = [_queueContents lastObject];
    if (singleQueueItem) {
        _queueContents = [NSMutableArray arrayWithObject:singleQueueItem];
    }
    
    [_tableView reloadData];
}

- (IBAction)lockButtonPress:(id)sender
{
    if (!_lockButton.selected) {
        _lockButton.selected = YES;
        [_sampleReceiver stopReceiving];
    } else {
        _lockButton.selected = NO;
        [_sampleReceiver startReceiving];
        [self repopulateSampleQueue];
    }
}

- (IBAction)deleteButtonPress:(id)sender
{
    KZPQueueItem *queueItem = [[KZPInteractionState sharedInteractionState] fetchTemporaryObjectOfType:[KZPQueueItem class]];
    [queueItem requestDeletion];
    [[KZPInteractionState sharedInteractionState] queueSampleSelectionEnded];
}

#pragma mark - <UITableViewDataSource>


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_queueContents count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KZPQueueItem *queueItem = [_queueContents objectAtIndex:indexPath.row];
    
    if (queueItem.queueItemView) {
        return queueItem.queueItemView;
    } else {
        KZPQueueItemTableViewCell *cell = (KZPQueueItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"queueItemCell" forIndexPath:indexPath];
        queueItem.queueItemView = cell;
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _tableView.frame.size.height / MAX_SAMPLE_QUEUE_SIZE;
}


#pragma mark - <KZPSampleReceiverDelegate>


- (void)newSamplesReceived
{
    [self repopulateSampleQueue];
}

- (void)repopulateSampleQueue
{
    NSUInteger newQueueItems = 0;
    
    while ([_queueContents count] < MAX_SAMPLE_QUEUE_SIZE) {
        NSString *sampleID = [_sampleReceiver nextSampleID];
        if (sampleID) {
            KZPQueueItem *queueItem = [[KZPQueueItem alloc] initWithSampleID:sampleID];
            queueItem.delegate = self;
            [_queueContents insertObject:queueItem atIndex:0];
            newQueueItems++;
        } else {
            break;
        }
    }
    
    if (newQueueItems > 0) {
        [self refreshWithNewElementCount:newQueueItems];
    }
}

- (void)refreshWithNewElementCount:(NSUInteger)newElementCount
{
    [self.tableView beginUpdates];
    NSMutableArray *indexPaths = [NSMutableArray array];
    
    for (int i = 0; i < newElementCount; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPaths addObject:indexPath];
    }

    [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationLeft];
    [self.tableView endUpdates];
}


#pragma mark - <KZPQueueItemDelegate>


- (void)queueItemRequestedDeletion:(KZPQueueItem *)queueItem
{
    [self.tableView beginUpdates];
    NSUInteger row = [_queueContents indexOfObject:queueItem];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    [_queueContents removeObject:queueItem];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    [self.tableView endUpdates];
    
    [self repopulateSampleQueue];
}


#pragma - <KZPInteractionStateObserver>


- (void)interactionStateDidChange
{
    KZPAppUsageMode appUsageMode = [[KZPInteractionState sharedInteractionState] appUsageMode];
    _deleteButton.enabled = (appUsageMode == KZPAppUsageModeSampleQueue);
}

@end
