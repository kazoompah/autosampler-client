//
//  KZPEffectCollectionViewCell.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 5/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPEffectCollectionViewCell.h"

@interface KZPEffectCollectionViewCell ()

@property (strong, nonatomic) UIColor *colorOn;
@property (strong, nonatomic) UIColor *colorSelected;

@end

@implementation KZPEffectCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.layer.shadowRadius = 4;
    self.layer.shadowOpacity = 0.3;
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(0, 3);
    self.layer.cornerRadius = 5.0;
    
    _colorOn = [UIColor colorWithRed:0.0 green:0.7 blue:0.0 alpha:1.0];
    _colorSelected = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:1.0];
    
    [self refresh];
}

- (IBAction)effectButtonPressed:(UIButton *)sender
{
    if ([self isSelected]) {
        [self setSelected:NO];
        _on = NO;
    } else {
        [self setSelected:YES];
        _on = YES;
    }
    [_delegate effectCellStateDidChange:self];
    [self refresh];
}

- (void)setOn:(BOOL)on
{
    _on = on;
    [self refresh];
}

- (void)deselect
{
    [self setSelected:NO];
    [self refresh];
}

- (void)refresh
{
    UIColor *backgroundColor;
    
    if (![self isOn]) {
        backgroundColor = [UIColor lightGrayColor];
        _effectLabel.textColor = [UIColor darkGrayColor];
    } else {
        backgroundColor = [self isSelected] ? _colorSelected : _colorOn;
        _effectLabel.textColor = [self isSelected] ? [UIColor darkGrayColor] : [UIColor whiteColor];
    }
    
    self.layer.backgroundColor = backgroundColor.CGColor;
}


@end
