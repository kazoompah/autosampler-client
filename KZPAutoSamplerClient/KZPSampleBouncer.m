//
//  KZPSampleBounce.m
//  KZPAutoSamplerClient
//
//  Created by Matt Rankin on 8/11/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPSampleBouncer.h"
#import "KZPSampleFileManager.h"

@interface KZPSampleBouncer ()

@property (weak, nonatomic) AVAudioPlayerNode *player; // need 'schedule buffer', thus we don't have the buffer
@property (weak, nonatomic) AVAudioNode *tapNode;
@property (strong, nonatomic) NSString *bounceName;
@property (strong, nonatomic) NSString *bouncedSampleFilePath;
@property (strong, nonatomic) AVAudioFile *bounceFile;

@property (copy) void (^completion)(void);
@property (weak, nonatomic) KZPDrumSurfaceButton *drumSurfaceButton;

@end

@implementation KZPSampleBouncer

- (instancetype)initWithDrumSurfaceButton:(KZPDrumSurfaceButton *)drumSurfaceButton
{
    self = [super init];
    if (self) {
        _drumSurfaceButton = drumSurfaceButton;
        [self extractSampleInformation];
    }
    return self;
}

- (void)extractSampleInformation
{
    _bounceName = [_drumSurfaceButton label];
    NSString *docs = [[KZPSampleFileManager sharedFileManager] documentsPath];
    NSString *sampleFilename = [_bounceName stringByAppendingPathExtension:@"wav"];
    _bouncedSampleFilePath = [docs stringByAppendingPathComponent:sampleFilename];
    _bounceURL = [NSURL fileURLWithPath:_bouncedSampleFilePath];
}

- (void)bounceWithCompletion:(void (^)())completion
{
    _completion = completion;
    
    KZPSample *sample = [_drumSurfaceButton sample];
    [self deletePreviousBounce];
    [self createBounceFile];
    [self installTap];
    
    [sample restartPlaybackIgnoreLoop:YES];
}

- (void)deletePreviousBounce
{
    NSError *error;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:_bouncedSampleFilePath]) {
        if (![[NSFileManager defaultManager] removeItemAtPath:_bouncedSampleFilePath error:&error]) {
            NSLog(@"Failed to delete file: %@: %@", _bouncedSampleFilePath, [error localizedDescription]);
        }
    }
}

- (void)createBounceFile
{
    NSError *error = nil;
    AudioStreamBasicDescription asbd = {0};
    asbd.mSampleRate = 44100.0;
    asbd.mFormatID = kAudioFormatLinearPCM;
    asbd.mFormatFlags = 0;
    asbd.mChannelsPerFrame = 1;
    asbd.mBitsPerChannel = 16;
    asbd.mBytesPerFrame = 2;
    
    AVAudioFormat *format = [[AVAudioFormat alloc] initWithStreamDescription:&asbd];
    
    _bounceFile = [[AVAudioFile alloc] initForWriting:_bounceURL settings:format.settings error:&error];
    if (error) {
        NSLog(@"Failed to create bounce file: %@", [error localizedDescription]);
    }
}

- (void)installTap
{
    KZPSample *sample = _drumSurfaceButton.sample;
    KZPEffectsChain *effectsChain = [_drumSurfaceButton effectsChain];
    if ([effectsChain effectsChainOutputNode]) {
        _tapNode = [effectsChain effectsChainOutputNode];
    } else {
        _tapNode = [sample playerNode];
    }
    
    [_tapNode installTapOnBus:0 bufferSize:4096 format:[sample format] block:^(AVAudioPCMBuffer * _Nonnull buffer, AVAudioTime * _Nonnull when) {
        
        NSError *error = nil;
        
        // TODO: use a proper processing technique (RMS etc) and then use the same kind of tap to determine when buttons are highlighted or not.
        
        Float32 *samples = [buffer audioBufferList]->mBuffers[0].mData;
        float total = 0;
        for (int i = 0; i < buffer.frameLength; i++) {
            total += fabs(samples[i]);
        }
        
        if (total < 1.0) {
            [self finishBounce];
        } else {
            if (![_bounceFile writeFromBuffer:buffer error:&error]) {
                NSLog(@"Failed to write buffer to file %@: %@", _bounceName, [error localizedDescription]);
            }
        }
        
    }];
}

- (void)finishBounce
{
    [_tapNode removeTapOnBus:0];
    self.bounceFile = nil;
    _completion();
}

@end
