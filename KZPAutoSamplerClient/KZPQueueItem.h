//
//  KZPQueueItem.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 19/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KZPSample.h"
#import "KZPQueueItemTableViewCell.h"

/* 
     The QueueSample object manages the download and audition playback for
     a single sample. 

 TODO: It should own a weak copy of its view just like the drum pad button
 the patterns are very similar, and the current code has become too complex with interaction state changes
 
 */

@class KZPQueueItem;
@protocol KZPQueueItemDelegate <NSObject>

- (void)queueItemRequestedDeletion:(KZPQueueItem *)queueItem;

@end

@interface KZPQueueItem : NSObject

@property (weak, nonatomic) id<KZPQueueItemDelegate> delegate;

@property (weak, nonatomic) KZPQueueItemTableViewCell *queueItemView;
@property (strong, nonatomic, readonly) KZPSample *sample;

- (instancetype)initWithSampleID:(NSString *)sampleID;
- (void)requestDeletion;

@end
