//
//  KZPAudioEngine.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 21/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KZPSample.h"
#import "KZPEffectsChain.h"
#import "KZPAudioEffect.h"

@interface KZPAudioEngine : NSObject

+ (KZPAudioEngine *)sharedEngine;

- (void)detachSample:(KZPSample *)sample;
- (void)detachEffect:(KZPAudioEffect *)effect;
- (void)connectEffect:(KZPAudioEffect *)effect1
             toEffect:(KZPAudioEffect *)effect2
               format:(AVAudioFormat *)effectFormat;

- (void)activateSample:(KZPSample *)sample
             onChannel:(AVAudioNodeBus)channelNumber
      withEffectsChain:(KZPEffectsChain *)effectsChain;

- (void)mute;
- (void)unmute;

@property (strong, nonatomic, readonly) AVAudioEngine *engine;

@end
