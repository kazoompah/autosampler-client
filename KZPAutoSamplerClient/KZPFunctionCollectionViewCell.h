//
//  KZPFunctionCollectionViewCell.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 26/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KZPFunctionCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *functionButton;

@end
