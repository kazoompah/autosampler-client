//
//  KZPTapeSpeedEffect.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 6/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPTapeSpeedEffect.h"

@interface KZPTapeSpeedEffect ()

@property (strong, nonatomic) AVAudioUnitVarispeed *varispeedUnit;

@end

@implementation KZPTapeSpeedEffect

- (instancetype)init
{
    self = [super init];
    if (self) {
        AudioComponentDescription varispeedComponentDescription;
        varispeedComponentDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
        varispeedComponentDescription.componentType = kAudioUnitType_FormatConverter;
        varispeedComponentDescription.componentSubType = kAudioUnitSubType_Varispeed;
        _varispeedUnit = [[AVAudioUnitVarispeed alloc] initWithAudioComponentDescription:varispeedComponentDescription];
    }
    return self;
}

- (float)continuousValue
{
    return log2f(_varispeedUnit.rate) / 4 + 0.5;
}

- (void)applyContinuousValue:(float)value
{
    _varispeedUnit.rate = powf(2, (value - 0.5) / 0.25);
}

- (AVAudioUnit *)audioNode
{
    return _varispeedUnit;
}

@end
