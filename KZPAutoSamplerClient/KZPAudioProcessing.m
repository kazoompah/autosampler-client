//
//  KZPAudioProcessing.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 2/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPAudioProcessing.h"


Float32 bias(Float32 *samples, uint32_t nSamples)
{
    Float32 total = 0.0;
    
    for (uint32_t i = 0; i < nSamples; i++) {
        Float32 amplitude = samples[i];
        total += amplitude;
    }
    
    return total / nSamples;
}

Float32 peak(Float32 *samples, uint32_t nSamples)
{
    Float32 peak = 0.0;
    
    for (uint32_t i = 0; i < nSamples; i++) {
        Float32 amplitude = fabs(samples[i]);
        if (amplitude > peak) {
            peak = amplitude;
        }
    }
    
    return peak;
}


@implementation KZPAudioProcessing

+ (void)reverseSamples:(Float32 *)samples nSamples:(UInt32)nSamples
{
    int i = 0;
    int j = nSamples - 1;
    
    while (j > i) {
        Float32 tmp = samples[j];
        samples[j] = samples[i];
        samples[i] = tmp;
        j--;
        i++;
    }
}

+ (void)correctBiasForSamples:(Float32 *)samples nSamples:(UInt32)nSamples
{
    float sampleBias = bias(samples, nSamples);
    
    for (uint32_t i = 0; i < nSamples; i++) {
        Float32 sample = samples[i];
        samples[i] = sample - sampleBias;
    }
}

+ (void)normalizeSamples:(Float32 *)samples nSamples:(UInt32)nSamples
{
    Float32 gain = 0.5 / peak(samples, nSamples);
    
    for (uint32_t i = 0; i < nSamples; i++) {
        Float32 sample = samples[i];
        samples[i] = sample * gain;
    }
}

@end
