//
//  KZPStatusBarView.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 26/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DOT_CYCLE_INTERVAL      0.66

@interface KZPStatusBarView : UIView

- (void)reset;
- (void)hidePollIndicator;
- (void)registerPoll;
- (void)changeStatusViewColor:(UIColor *)targetColor;
- (void)hideStatusLabel;
- (void)updateStatusLabel:(NSString *)statusText;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *pollingDots;

@end
