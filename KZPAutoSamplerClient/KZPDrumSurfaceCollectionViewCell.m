//
//  KZPDrumSurfaceCollectionViewCell.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 20/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPDrumSurfaceCollectionViewCell.h"

@interface KZPDrumSurfaceCollectionViewCell ()

@property (strong, nonatomic) NSURL *currentAudioURL;

@property (strong, nonatomic) UIImage *loopingImage;
@property (strong, nonatomic) UIImage *holdImage;

@end

@implementation KZPDrumSurfaceCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _padImage.layer.shadowRadius = 4;
    _padImage.layer.shadowOpacity = 0.3;
    _padImage.layer.masksToBounds = NO;
    _padImage.layer.shadowOffset = CGSizeMake(0, 3);
    _padImage.layer.cornerRadius = 8.0;
    _waveformView.doesAllowScrubbing = NO;
    _waveformView.doesAllowStretch = NO;
    _waveformView.doesAllowScroll = NO;
    _waveformView.wavesColor = [UIColor darkGrayColor];
    _performanceStyleIndicator1.alpha = 0.5;
    _performanceStyleIndicator2.alpha = 0.5;
    _performanceStyleExclusiveIndicator.alpha = 0.5;
    _effectsIndicator.alpha = 0.5;
    _loopingImage = [_performanceStyleIndicator1 image];
    _holdImage = [_performanceStyleIndicator2 image];
    [self updatePerformanceIndicatorsWithLoop:NO hold:NO exclusive:NO];
    [self showEffectsIndicator:NO];
    [self displayState:KZPDrumSurfaceButtonStateInactive];
}

- (IBAction)drumSurfaceButtonTouchDown:(id)sender
{
    [_delegate drumSurfaceButtonEngaged];
}

- (IBAction)drumSurfaceButtonTouchUpInside:(id)sender
{
    [_delegate drumSurfaceButtonReleased];
}

- (void)displayState:(KZPDrumSurfaceButtonState)buttonState
{
    [self.loadingIndicator stopAnimating];
    
    UIColor *stateColor;
    switch (buttonState) {
        case KZPDrumSurfaceButtonStateInactive: stateColor = [UIColor lightGrayColor]; break;
        case KZPDrumSurfaceButtonStateLoading:
            stateColor = [UIColor lightGrayColor];
            [_loadingIndicator startAnimating];
            break;
        case KZPDrumSurfaceButtonStateActive: stateColor = [UIColor orangeColor]; break;
        case KZPDrumSurfaceButtonStateHighlighted: stateColor = [UIColor yellowColor]; break;
        case KZPDrumSurfaceButtonStateEditing: stateColor = [UIColor cyanColor]; break;
        case KZPDrumSurfaceButtonStateError: stateColor = [UIColor redColor]; break;
        default: stateColor = [UIColor lightGrayColor]; break;
    }
    _padImage.backgroundColor = stateColor;
}

- (void)displayWaveformForAudioFileURL:(NSURL *)audioFileURL
{
    if (_currentAudioURL != audioFileURL) {
        _currentAudioURL = audioFileURL;
        if (audioFileURL) {
            _waveformView.audioURL = audioFileURL;
        }
    }
    _waveformView.hidden = (audioFileURL == nil);
}

- (void)updatePerformanceIndicatorsWithLoop:(BOOL)loop hold:(BOOL)hold exclusive:(BOOL)exclusive
{
    _performanceStyleExclusiveIndicator.hidden = !exclusive;
    
    _performanceStyleIndicator1.hidden = YES;
    _performanceStyleIndicator2.hidden = YES;
    
    if (loop) {
        _performanceStyleIndicator1.hidden = NO;
        _performanceStyleIndicator1.image = _loopingImage;
    }
    
    if (hold) {
        UIImageView *holdImageView = loop ? _performanceStyleIndicator2 : _performanceStyleIndicator1;
        holdImageView.hidden = NO;
        holdImageView.image = _holdImage;
    }
}

- (void)showEffectsIndicator:(BOOL)show
{
    _effectsIndicator.hidden = !show;
}

@end
