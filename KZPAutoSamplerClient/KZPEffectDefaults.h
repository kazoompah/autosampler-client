//
//  KZPEffectDefaults.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 6/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KZPEffectDefaults : NSObject

+ (KZPEffectDefaults *)sharedDefaults;

- (float)defaultValueForEffect:(NSString *)effectID;

@end
