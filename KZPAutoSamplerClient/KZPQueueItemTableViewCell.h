//
//  KZPQueueItemTableViewCell.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 18/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KZPQueueItemTableViewCellDelegate <NSObject>

- (void)queueItemPlayButtonPressed;

@end

@interface KZPQueueItemTableViewCell : UITableViewCell

@property (weak, nonatomic) id<KZPQueueItemTableViewCellDelegate> delegate;

- (void)reset;
- (void)displayReadyState:(BOOL)ready;
- (void)displayActiveState:(BOOL)active;
- (void)displayStandbyState:(BOOL)standby;
- (void)displayWaveformForAudioFileURL:(NSURL *)audioFileURL;

@end
