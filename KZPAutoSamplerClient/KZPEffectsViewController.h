//
//  KZPEffectsViewController.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 4/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KZPEffectsChain.h"

@protocol KZPEffectsViewControllerDelegate <NSObject>

- (void)effectsSettingsDidChange;

@end

@interface KZPEffectsViewController : UIViewController

@property (weak, nonatomic) id<KZPEffectsViewControllerDelegate> delegate;

@property (weak, nonatomic) KZPEffectsChain *effectsChain;

@end
