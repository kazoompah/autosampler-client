//
//  KZPAudioEffect.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 5/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

@import AVFoundation;

@interface KZPAudioEffect : NSObject

// Override
- (AVAudioUnit *)audioNode;

// Override
- (float)continuousValue;

// Override
- (void)applyContinuousValue:(float)value;

@property (weak, nonatomic) KZPAudioEffect *downstreamEffect;
@property (weak, nonatomic) KZPAudioEffect *upstreamEffect;

@end
