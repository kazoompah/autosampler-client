//
//  main.m
//  KZPAutoSamplerClient
//
//  Created by Matt Rankin on 17/08/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KZPAutoSamplerAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KZPAutoSamplerAppDelegate class]));
    }
}
