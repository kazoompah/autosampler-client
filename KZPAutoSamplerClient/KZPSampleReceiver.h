//
//  KZPSampleReceiver.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 28/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

#define INCOMING_SAMPLE_LIFETIME    90.0

@protocol KZPSampleReceiverDelegate <NSObject>

- (void)newSamplesReceived;

@end

@interface KZPSampleReceiver : NSObject

@property (weak, nonatomic) id<KZPSampleReceiverDelegate> delegate;

- (void)reset;
- (NSString *)nextSampleID;

- (void)stopReceiving;
- (void)startReceiving;

@end
