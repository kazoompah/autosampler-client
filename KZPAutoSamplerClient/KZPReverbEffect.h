//
//  KZPReverbEffect.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 6/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPAudioEffect.h"

#define MAX_REVERB_TIME         10.0;
#define DEFAULT_REVERB_SETTING  0.3;

@interface KZPReverbEffect : KZPAudioEffect

@end
