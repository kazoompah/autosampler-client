//
//  KZPSample.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 26/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreAudio/CoreAudioTypes.h>

@import AVFoundation;

@protocol KZPSampleDelegate <NSObject>

- (void)sampleDidFinishPlaying;
- (void)sampleDidStartPlaying;

@end

@interface KZPSample : NSObject

@property (weak, nonatomic) id<KZPSampleDelegate> delegate;

+ (KZPSample *)sampleWithFileURL:(NSURL *)audioFileURL;
+ (KZPSample *)sampleWithSample:(KZPSample *)sample;

- (void)loadWithCompletion:(void (^)(void))completionBlock failure:(void (^)(void))failureBlock;
- (AVAudioFormat *)format;
- (void)restartPlaybackIgnoreLoop:(BOOL)ignoreLoop;
- (void)stopPlayback;

@property (strong, nonatomic, readonly) NSURL *audioFileURL;
@property (strong, nonatomic, readonly) AVAudioPlayerNode *playerNode;

@property (nonatomic, readonly, getter=isLoaded) BOOL loaded;
@property (nonatomic, readonly, getter=isPlaying) BOOL playing;
@property (nonatomic) BOOL shouldLoop;
@property (nonatomic) BOOL shouldPlayInReverse;

@end
