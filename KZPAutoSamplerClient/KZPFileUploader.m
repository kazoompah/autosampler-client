//
//  KZPFileUploader.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 2/11/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPFileUploader.h"
#import "AFNetworking.h"
#import "KZPSampleFileManager.h"
#import "KZPNetworkConstants.h"

@implementation KZPFileUploader

- (void)setFilename:(NSString *)filename
{
    _filename = filename;
    if (_filename) {
        [self generateFilePath];
    }
}

- (void)upload
{
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST"
                                                                                              URLString:_remotePath
                                                                                             parameters:nil
                                                                              constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                                                  
        [formData appendPartWithFileURL:_localFileURL
                                   name:[_filename stringByDeletingPathExtension]
                               fileName:_filename mimeType:@"application/zip" error:nil];
                                                                                  
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [request setTimeoutInterval:SERVER_POST_TIMEOUT];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager uploadTaskWithStreamedRequest:request
                  
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      [_delegate fileUploadProgress:uploadProgress.fractionCompleted];
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                      
                      if (httpResponse.statusCode != 200) {
                          [_delegate fileUploadFailed];
                          NSLog(@"Error: %@", error);
                      } else {
                          [_delegate fileUploadComplete];
                      }
                  }];
    
    [uploadTask resume];
}

- (void)generateFilePath
{
    NSString *documents = [[KZPSampleFileManager sharedFileManager] documentsPath];
    NSString *filePath = [documents stringByAppendingPathComponent:_filename];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        _localFilePath = filePath;
        _localFileURL = [NSURL fileURLWithPath:_localFilePath];
    }
}

@end
