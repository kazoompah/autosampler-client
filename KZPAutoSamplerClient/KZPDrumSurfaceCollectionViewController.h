//
//  KZPDrumSurfaceCollectionViewController.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 20/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KZPDrumSurfaceCollectionViewController : UICollectionViewController

- (NSArray *)generateSampleBouncers;
- (void)resetAllButtons;

@end
