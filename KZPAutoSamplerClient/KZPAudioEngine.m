//
//  KZPAudioEngine.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 21/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPAudioEngine.h"

@import AVFoundation;

static KZPAudioEngine *sharedInstance;

@interface KZPAudioEngine ()

@property (strong, nonatomic) AVAudioMixerNode *mixer;

@end

@implementation KZPAudioEngine

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSError *error = nil;
        [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error:&error];
        if (error) {
            NSLog(@"Error setting category! %@", [error localizedDescription]);
        }
        
        [[AVAudioSession sharedInstance] setPreferredOutputNumberOfChannels:1 error:&error];
        if (error) {
            NSLog(@"Error setting channel number! %@", [error localizedDescription]);
        }
        
        _engine = [[AVAudioEngine alloc] init];
        _mixer = [_engine mainMixerNode];
        
        [_engine startAndReturnError:&error];
        if (error) {
            NSLog(@"Error starting audio engine: %@", [error localizedDescription]);
        }
    }
    return self;
}

+ (KZPAudioEngine *)sharedEngine
{
    if (!sharedInstance) sharedInstance = [[KZPAudioEngine alloc] init];
    return sharedInstance;
}

- (void)activateSample:(KZPSample *)sample
             onChannel:(AVAudioNodeBus)channelNumber
      withEffectsChain:(KZPEffectsChain *)effectsChain
{
    if (![sample.playerNode engine]) {
        [_engine attachNode:sample.playerNode];
    }
    
    [_engine disconnectNodeOutput:sample.playerNode];
    [_engine disconnectNodeInput:_mixer bus:channelNumber];
    
    if (![effectsChain effectsChainInputNode] || ![effectsChain effectsChainOutputNode]) {
        [_engine connect:sample.playerNode to:_mixer fromBus:0 toBus:channelNumber format:sample.format];
    } else {
        [_engine connect:sample.playerNode to:[effectsChain effectsChainInputNode] fromBus:0 toBus:0 format:sample.format];
        [_engine connect:[effectsChain effectsChainOutputNode] to:_mixer fromBus:0 toBus:channelNumber format:sample.format];
    }
}

- (void)detachSample:(KZPSample *)sample
{
    if ([sample.playerNode engine] == nil) return;
    [_engine disconnectNodeOutput:sample.playerNode];
    [_engine detachNode:sample.playerNode];
}

- (void)connectEffect:(KZPAudioEffect *)effect1
             toEffect:(KZPAudioEffect *)effect2
               format:(AVAudioFormat *)effectFormat
{
    [self attachEffect:effect1];
    [self attachEffect:effect2];
    
    if (!effectFormat || !effect1 || !effect2) return;
    
    [_engine disconnectNodeOutput:[effect1 audioNode]];
    [_engine disconnectNodeInput:[effect2 audioNode]];
    [_engine connect:[effect1 audioNode] to:[effect2 audioNode] format:effectFormat];
}

- (void)attachEffect:(KZPAudioEffect *)effect
{
    AVAudioNode *audioNode = [effect audioNode];
    if (audioNode && ![audioNode engine]) {
        [_engine attachNode:audioNode];
    }
}

- (void)detachEffect:(KZPAudioEffect *)effect
{
    AVAudioNode *audioNode = [effect audioNode];
    if ([audioNode engine] == nil) return;
    [_engine disconnectNodeInput:audioNode];
    [_engine disconnectNodeOutput:audioNode];
    [_engine detachNode:audioNode];
}

- (void)mute
{
    [_mixer setOutputVolume:0.0];
}

- (void)unmute
{
    [_mixer setOutputVolume:1.0];
}

@end
