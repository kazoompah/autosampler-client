//
//  KZPServerQueryService.m
//  KZPAutoSamplerClient
//
//  Created by Matt Rankin on 17/08/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPServerQueryService.h"
#import "AFNetworking.h"
#import "KZPNetworkConstants.h"

@interface KZPServerQueryService ()

@property (strong, nonatomic) NSString *lastTimestamp;
@property (strong, nonatomic) NSTimer *pollingTimer;
@property (nonatomic) BOOL cancelled;

@end

@implementation KZPServerQueryService

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.lastTimestamp = [[NSUserDefaults standardUserDefaults] valueForKey:kSERVER_RESPONSE_LAST_TIMESTAMP];
        if (!self.lastTimestamp) {
            self.lastTimestamp = @"0";
        }
    }
    return self;
}

- (void)startPolling
{
    _cancelled = NO;
    [self startPollingTimer];
}

- (void)stopPolling
{
    _cancelled = YES;
    [self.pollingTimer invalidate];
    self.pollingTimer = nil;
}

- (void)startPollingTimer
{
    self.pollingTimer = [NSTimer scheduledTimerWithTimeInterval:SERVER_QUERY_INTERVAL
                                                         target:self
                                                       selector:@selector(pollServer)
                                                       userInfo:nil
                                                        repeats:YES];
}

- (void)pollServer
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = SERVER_QUERY_TIMEOUT;
    
    [manager GET:[_remoteQueryPath stringByAppendingPathComponent:_lastTimestamp]
      parameters:nil
        progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
        if (!_cancelled) [self pollSuccessWithResponse:(NSDictionary *)responseObject];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        if (!_cancelled) [self pollFailure];
    }];
}

- (void)pollSuccessWithResponse:(NSDictionary *)response
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        if ([self checkTimestamp:[response valueForKey:kSERVER_RESPONSE_LAST_TIMESTAMP]]) {
            NSArray *sampleList = [response valueForKey:kSERVER_RESPONSE_SAMPLES];
            [self.delegate queryServiceReceivedSampleListUpdate:sampleList];
        } else {
            [self.delegate queryServiceHasConnection];
        }
    });
}

- (void)pollFailure
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [self.delegate queryServiceLostConnection];
    });
}

- (BOOL)checkTimestamp:(NSString *)timestamp
{
    if (![self.lastTimestamp isEqualToString:timestamp]) {
        [[NSUserDefaults standardUserDefaults] setValue:timestamp forKey:kSERVER_RESPONSE_LAST_TIMESTAMP];
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.lastTimestamp = timestamp;
        return YES;
    }
    return NO;
}

@end
