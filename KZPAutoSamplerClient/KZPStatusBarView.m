//
//  KZPStatusBarView.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 26/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPStatusBarView.h"

@interface KZPStatusBarView ()



@property (strong, nonatomic) UILabel *currentDot;
@property (strong, nonatomic) NSTimer *dotCycleTimer;
@property (nonatomic) NSUInteger pollsRegistered;

@end

@implementation KZPStatusBarView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self reset];
}

- (void)updateStatusLabel:(NSString *)statusText
{
    if (![statusText isEqualToString:self.statusLabel.text]) {
        self.statusLabel.text = statusText;
        [self showStatusLabel];
    }
}

- (void)changeStatusViewColor:(UIColor *)targetColor
{
    [UIView animateWithDuration:0.6 animations:^{
        [self setBackgroundColor:targetColor];
    }];
}

- (void)hideStatusLabel
{
    [UIView animateWithDuration:0.6 animations:^{
        self.statusLabel.alpha = 0.0;
    } completion:^(BOOL finished) {
        self.statusLabel.hidden = YES;
    }];
}

- (void)showStatusLabel
{
    self.statusLabel.hidden = NO;
    self.statusLabel.alpha = 0.0;
    [UIView animateWithDuration:0.6 animations:^{
        self.statusLabel.alpha = 1.0;
    }];
}

- (void)reset
{
    self.statusLabel.hidden = YES;
    [self hidePollIndicatorAnimated:NO];
}

- (void)registerPoll
{
    self.pollsRegistered++;
    if (!self.dotCycleTimer) {
        self.dotCycleTimer = [NSTimer scheduledTimerWithTimeInterval:DOT_CYCLE_INTERVAL
                                                              target:self
                                                            selector:@selector(pollCycleTimerFired)
                                                            userInfo:nil
                                                             repeats:YES];
        [self cyclePollDots];
    }
}

- (void)pollCycleTimerFired
{
    if (self.pollsRegistered > 0) {
        [self cyclePollDots];
        self.pollsRegistered = 0;
    }
}

- (void)cyclePollDots
{
    UILabel *previousDot = self.currentDot;
    self.currentDot = [self nextDot];
    
    [UIView animateWithDuration:0.7 animations:^{
        previousDot.alpha = 0.0;
        self.currentDot.alpha = 1.0;
    }];
}

- (UILabel *)nextDot
{
    NSInteger currentTag = self.currentDot ? self.currentDot.tag : 0;
    NSInteger nextTag = (currentTag + 1) % [self.pollingDots count];
    
    for (UILabel *dot in self.pollingDots) {
        if (dot.tag == nextTag) {
            return dot;
        }
    }
    
    return nil;
}

- (void)hidePollIndicator
{
    [self hidePollIndicatorAnimated:YES];
    [self.dotCycleTimer invalidate];
    self.dotCycleTimer = nil;
}

- (void)hidePollIndicatorAnimated:(BOOL)animated
{
    [UIView animateWithDuration:animated ? 0.2 : 0.0 animations:^{
        for (UILabel *dot in self.pollingDots) {
            dot.alpha = 0.0;
        }
    }];
}


@end
