//
//  KZPFunctionsViewController.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 18/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KZPFunctionsDelegate <NSObject>

- (void)exportFunctionSelected;
- (void)resetFunctionSelected;

@end

@interface KZPFunctionsCollectionViewController : UICollectionViewController

@property (weak, nonatomic) id<KZPFunctionsDelegate> delegate;

@end
