//
//  KZPDelayEffect.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 6/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPDelayEffect.h"

@interface KZPDelayEffect ()

@property (strong, nonatomic) AVAudioUnitDelay *delayUnit;

@end

@implementation KZPDelayEffect

- (instancetype)init
{
    self = [super init];
    if (self) {
        AudioComponentDescription delayComponentDescription;
        delayComponentDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
        delayComponentDescription.componentType = kAudioUnitType_Effect;
        delayComponentDescription.componentSubType = kAudioUnitSubType_Delay;
        _delayUnit = [[AVAudioUnitDelay alloc] initWithAudioComponentDescription:delayComponentDescription];
    }
    return self;
}

- (void)applyContinuousValue:(float)value
{
    _delayUnit.delayTime = (NSTimeInterval)value * 2;
    _delayUnit.feedback = value * 100;
}

- (float)continuousValue
{
    return _delayUnit.delayTime / 2;
}

- (AVAudioUnit *)audioNode
{
    return _delayUnit;
}

@end
