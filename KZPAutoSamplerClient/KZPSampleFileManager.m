//
//  KZPSampleFileManager.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 21/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPSampleFileManager.h"

static KZPSampleFileManager *sharedInstance;

@interface KZPSampleFileManager ()

@property (strong, nonatomic) NSMutableDictionary *sampleReferences;

@end

@implementation KZPSampleFileManager

+ (KZPSampleFileManager *)sharedFileManager
{
    if (!sharedInstance) sharedInstance = [[KZPSampleFileManager alloc] init];
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _sampleReferences = [NSMutableDictionary dictionary];
    }
    return self;
}

- (NSString *)documentsPath
{
    const char *path = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] fileSystemRepresentation];
    
    NSString *documents = [[NSFileManager defaultManager] stringWithFileSystemRepresentation:path length:strlen(path)];
    
    return documents;
}

- (void)sampleOwner:(id)owner refersToSampleAtFileURL:(NSURL *)fileURL
{
    NSString *filePath = [fileURL path];
    NSString *ownerName = [owner description];
    NSMutableArray *owners = [_sampleReferences valueForKey:filePath];
    if (owners) {
        [owners addObject:ownerName];
    } else {
        [_sampleReferences setValue:[NSMutableArray arrayWithObject:ownerName] forKey:filePath];
    }
}

- (void)sampleOwner:(id)owner attemptToRemoveSampleAtFileURL:(NSURL *)fileURL
{
    NSString *filePath = [fileURL path];
    NSString *ownerName = [owner description];
    NSMutableArray *owners = [_sampleReferences valueForKey:filePath];
    [owners removeObject:ownerName];
    
    if ([owners count] == 0) {
        [self deleteFileAtPath:filePath];
        [_sampleReferences removeObjectForKey:filePath];
    }
}

- (void)deleteFileAtPath:(NSString *)filePath
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSError *error;
        if (![[NSFileManager defaultManager] removeItemAtPath:filePath error:&error]) {
            NSLog(@"Error: failed to delete %@: %@", filePath, [error localizedDescription]);
        } else {
            NSLog(@"Deleted %@", filePath);
        }
    }
}

@end
