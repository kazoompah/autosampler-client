//
//  KZPQueueViewController.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 18/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KZPQueueItem.h"

#define MAX_SAMPLE_QUEUE_SIZE   7

@interface KZPQueueViewController : UIViewController

@end
