//
//  KZPEffectCollectionViewCell.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 5/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KZPEffectCollectionViewCell;
@protocol KZPEffectCellDelegate <NSObject>

- (void)effectCellStateDidChange:(KZPEffectCollectionViewCell *)effectCell;

@end

@interface KZPEffectCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) id<KZPEffectCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *effectImageView;
@property (weak, nonatomic) IBOutlet UILabel *effectLabel;
@property (strong, nonatomic) NSString *effectID;

@property (nonatomic, getter=isOn) BOOL on;

- (void)deselect;

@end
