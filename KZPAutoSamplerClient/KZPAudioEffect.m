//
//  KZPAudioEffect.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 5/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPAudioEffect.h"

@implementation KZPAudioEffect

- (void)setDownstreamEffect:(KZPAudioEffect *)downstreamEffect
{
    if (downstreamEffect != self) {
        _downstreamEffect = downstreamEffect;
    }
}

- (void)setUpstreamEffect:(KZPAudioEffect *)upstreamEffect
{
    if (upstreamEffect != self) {
        _upstreamEffect = upstreamEffect;
    }
}

- (float)continuousValue
{
    NSAssert(NO, @"Must override continuousValue for %@", [self class]);
    return 0;
}

- (void)applyContinuousValue:(float)value
{
    NSAssert(NO, @"Must override applyContinuousValue for %@", [self class]);
}

- (AVAudioUnit *)audioNode
{
    NSAssert(NO, @"Must override effectNode for %@", [self class]);
    return nil;
}

@end
