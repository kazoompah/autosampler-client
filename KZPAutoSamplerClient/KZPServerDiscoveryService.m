//
//  KZPServerDiscoverer.m
//  KZPAutoSamplerClient
//
//  Created by Matt Rankin on 17/08/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPServerDiscoveryService.h"
#import "KZPUDPDiscoveryService.h"

@interface KZPServerDiscoveryService () <KZPUDPDiscoveryDelegate>

@property (strong, nonatomic) NSTimer *failureTimer;
@property (strong, nonatomic) NSTimer *discoveryTimer;
@property (strong, nonatomic) KZPUDPDiscoveryService *discoveryService;

@end

@implementation KZPServerDiscoveryService

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.discoveryService = [[KZPUDPDiscoveryService alloc] init];
        self.discoveryService.delegate = self;
    }
    return self;
}

- (void)startDiscovery
{
    self.discoveryTimer = [NSTimer scheduledTimerWithTimeInterval:SERVER_DISCOVERY_INTERVAL
                                                           target:self.discoveryService
                                                         selector:@selector(sendDiscoveryBroadcast)
                                                         userInfo:nil
                                                          repeats:YES];
    
    self.failureTimer = [NSTimer scheduledTimerWithTimeInterval:SERVER_DISCOVERY_TIMEOUT
                                                         target:self
                                                       selector:@selector(discoveryTimeout)
                                                       userInfo:nil
                                                        repeats:NO];
}

- (void)discoveryTimeout
{
    [self.delegate serverCannotBeFound];
}

- (void)stopDiscovery
{
    [self.failureTimer invalidate];
    [self.discoveryTimer invalidate];
    self.failureTimer = nil;
    self.discoveryTimer = nil;
}


#pragma mark - KZPUDPDiscoveryDelegate -


- (void)discoverySucceededWithIP:(NSString *)serverIP
{
    [self.discoveryService cancel];
    [self.delegate serverDiscoveredAtIP:serverIP];
}

- (void)discoveryFailedWithMessage:(NSString *)message abort:(BOOL)abort
{
    if (abort) {
        [self.discoveryService cancel];
    }
    [self.delegate serverDiscoveryFailedWithMessage:message abort:abort];
}


@end
