//
//  KZPPitchShiftEffect.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 6/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPPitchShiftEffect.h"

@interface KZPPitchShiftEffect ()

@property (strong, nonatomic) AVAudioUnitTimePitch *pitchShiftUnit;

@end

@implementation KZPPitchShiftEffect

- (instancetype)init
{
    self = [super init];
    if (self) {
        AudioComponentDescription pitchShiftComponentDescription;
        pitchShiftComponentDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
        pitchShiftComponentDescription.componentType = kAudioUnitType_FormatConverter;
        pitchShiftComponentDescription.componentSubType = kAudioUnitSubType_NewTimePitch;
        _pitchShiftUnit = [[AVAudioUnitTimePitch alloc] initWithAudioComponentDescription:pitchShiftComponentDescription];
    }
    return self;
}

- (void)applyContinuousValue:(float)value
{
    _pitchShiftUnit.pitch = (value - 0.5) * 2 * 2400;
}

- (float)continuousValue
{
    return _pitchShiftUnit.pitch / 2400 / 2 + 0.5;
}

- (AVAudioUnit *)audioNode
{
    return _pitchShiftUnit;
}

@end
