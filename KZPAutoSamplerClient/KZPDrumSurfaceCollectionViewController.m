//
//  KZPDrumSurfaceCollectionViewController.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 20/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPDrumSurfaceCollectionViewController.h"
#import "KZPInteractionState.h"
#import "KZPDrumSurfaceButton.h"
#import "KZPPersistentStore.h"
#import "KZPSampleBouncer.h"

@interface KZPDrumSurfaceCollectionViewController () <KZPDrumSurfaceButtonDelegate>

@property (strong, nonatomic) NSMutableDictionary *drumSurfaceButtons;
@property (nonatomic) CGSize surfaceSize;

@end

@implementation KZPDrumSurfaceCollectionViewController

static NSString * const reuseIdentifier = @"drumSurfaceCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _drumSurfaceButtons = [NSMutableDictionary dictionary];
    _surfaceSize = [[KZPPersistentStore sharedStore] bankDimensions];
    
    [[KZPInteractionState sharedInteractionState] drumSurfaceDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark <UICollectionViewDataSource>


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _surfaceSize.width * _surfaceSize.height;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    KZPDrumSurfaceCollectionViewCell *cell = (KZPDrumSurfaceCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    CGPoint cellDrumSurfaceLocation = [self drumSurfaceLocationWithIndexPath:indexPath];
    NSString *cellDrumSurfaceLocationKey = [KZPDrumSurfaceButton drumSurfaceLocationKeyWithLocation:cellDrumSurfaceLocation];
        
    KZPDrumSurfaceButton *drumSurfaceButton = self.drumSurfaceButtons[cellDrumSurfaceLocationKey];
    
    if (!drumSurfaceButton) {
        
        // TODO: Need to pass saved information for this location. Or, it could find it itself. But that
        // depends on how banks are managed.
        drumSurfaceButton = [[KZPDrumSurfaceButton alloc] initWithDrumSurfaceLocation:cellDrumSurfaceLocation savedDrumButtonInfo:nil];
        
        [self.drumSurfaceButtons setValue:drumSurfaceButton forKey:cellDrumSurfaceLocationKey];
    }
    
    drumSurfaceButton.delegate = self;
    drumSurfaceButton.drumSurfaceButtonView = cell;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)collectionViewLayout;
    
    CGSize drumSurfaceSize = self.view.frame.size;
    CGFloat drumSurfaceInsetWidth = drumSurfaceSize.width - flowLayout.sectionInset.left - flowLayout.sectionInset.right;
    CGFloat drumSurfaceInsetHeight = drumSurfaceSize.height - flowLayout.sectionInset.top - flowLayout.sectionInset.bottom;
    CGFloat drumSurfaceTotalPadWidth = drumSurfaceInsetWidth - (_surfaceSize.width - 1) * flowLayout.minimumInteritemSpacing;
    CGFloat drumSurfaceTotalPadHeight = drumSurfaceInsetHeight - (_surfaceSize.height - 1) * flowLayout.minimumLineSpacing;
    return CGSizeMake(drumSurfaceTotalPadWidth / _surfaceSize.width, drumSurfaceTotalPadHeight / _surfaceSize.height);
}

- (CGPoint)drumSurfaceLocationWithIndexPath:(NSIndexPath *)indexPath
{
    NSInteger location = indexPath.row;
    NSInteger xLocation = location % (NSInteger)_surfaceSize.width;
    NSInteger yLocation = location / (NSInteger)_surfaceSize.width;
    return CGPointMake(xLocation, yLocation);
}

- (NSArray *)generateSampleBouncers
{
    NSMutableArray *sampleBouncers = [NSMutableArray array];
    for (KZPDrumSurfaceButton *button in [_drumSurfaceButtons allValues]) {
        if (button.sample.audioFileURL) {
            KZPSampleBouncer *bouncer = [[KZPSampleBouncer alloc] initWithDrumSurfaceButton:button];
            [sampleBouncers addObject:bouncer];
        }
    }
    return sampleBouncers;
}

- (void)resetAllButtons
{
    for (KZPDrumSurfaceButton *button in [_drumSurfaceButtons allValues]) {
        [button reset];
    }
}


#pragma mark - <KZPDrumSurfaceButtonDelegate>


- (void)presentViewController:(UIViewController *)viewController
{
    viewController.popoverPresentationController.sourceView = [self view];
    [self presentViewController:viewController animated:YES completion:^{}];
}

- (void)drumSurfaceButtonWasTriggered:(KZPDrumSurfaceButton *)drumSurfaceButton
{
    for (KZPDrumSurfaceButton *button in [_drumSurfaceButtons allValues]) {
        if (button != drumSurfaceButton && (button.playsExclusive || drumSurfaceButton.playsExclusive)) {
            [button.sample stopPlayback];
        }
    }
}

@end
