//
//  KZPServerDiscoveryService.h
//  KZPAutoSamplerClient
//
//  Created by Matt Rankin on 17/08/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KZPNetworkConstants.h"


@protocol KZPServerDiscoveryServiceDelegate <NSObject>

- (void)serverDiscoveredAtIP:(NSString *)serverIP;
- (void)serverDiscoveryFailedWithMessage:(NSString *)message abort:(BOOL)abort;
- (void)serverCannotBeFound;

@end

@interface KZPServerDiscoveryService : NSObject

@property (weak, nonatomic) id<KZPServerDiscoveryServiceDelegate> delegate;

- (void)startDiscovery;
- (void)stopDiscovery;

@end
