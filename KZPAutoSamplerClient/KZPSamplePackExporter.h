//
//  KZPSamplePackExporter.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 2/11/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface KZPSamplePackExporter : NSObject

- (instancetype)initWithHUD:(MBProgressHUD *)hud;
- (void)bounceAndExportSamples:(NSArray *)sampleBouncers;

@end
