//
//  KZPEffectsViewController.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 4/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPEffectsViewController.h"
#import "KZPInteractionState.h"
#import "KZPEffectCollectionViewCell.h"
#import "KZPAudioEffect.h"

@interface KZPEffectsViewController () <UICollectionViewDataSource, UICollectionViewDelegate, KZPEffectCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *effectsCollectionView;

@property (weak, nonatomic) IBOutlet UISlider *effectSettingSlider;
@property (strong, nonatomic) NSArray *effectIDs;
@property (strong, nonatomic) NSMutableDictionary *effectCells;
@property (strong, nonatomic) NSString *selectedEffectID;

@end

@implementation KZPEffectsViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    _effectIDs = @[@"Reverb", @"Speed", @"Distortion", @"Stretch", @"Pitch", @"Delay"];
    _effectCells = [NSMutableDictionary dictionary];
    _effectsCollectionView.dataSource = self;
    _effectsCollectionView.delegate = self;
    _effectSettingSlider.value = 0.0;
    _effectSettingSlider.enabled = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[KZPInteractionState sharedInteractionState] functionDeselected];
}

- (IBAction)sliderValueChanged:(UISlider *)sender
{
    [_effectsChain applyContinuousValue:sender.value toEffect:_selectedEffectID];
    [_delegate effectsSettingsDidChange];
}


#pragma mark - <UICollectionViewDataSource>


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_effectIDs count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *effectID = _effectIDs[indexPath.row];
    NSString *reuseIdentifier = [NSString stringWithFormat:@"effectCell%@", effectID];
    
    KZPEffectCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    cell.effectID = effectID;
    cell.on = [[_effectsChain activeEffects] containsObject:effectID];
    
    _effectCells[effectID] = cell;
    
    return cell;
}


#pragma mark - <KZPEffectCellDelegate>


- (void)effectCellStateDidChange:(KZPEffectCollectionViewCell *)effectCell
{
    if ([effectCell isSelected]) {
        [self deselectEffectCellsExcept:effectCell];
        [_effectsChain activateEffect:effectCell.effectID withoutReconnecting:NO];
        _effectSettingSlider.enabled = YES;
        _effectSettingSlider.value = [_effectsChain continuousValueForEffect:effectCell.effectID];
        _selectedEffectID = effectCell.effectID;
        [_delegate effectsSettingsDidChange];
        
    } else {
        _effectSettingSlider.enabled = NO;
        _selectedEffectID = nil;
        [_effectsChain deactivateEffect:effectCell.effectID withoutReconnecting:NO];
        [_delegate effectsSettingsDidChange];
    }
}

- (void)deselectEffectCellsExcept:(KZPEffectCollectionViewCell *)effectCell
{
    for (KZPEffectCollectionViewCell *cell in [_effectCells allValues]) {
        if (cell != effectCell) {
            [cell deselect];
        }
    }
}

@end
