//
//  KZPSampleBounce.h
//  KZPAutoSamplerClient
//
//  Created by Matt Rankin on 8/11/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KZPSample.h"
#import "KZPDrumSurfaceButton.h"

@import AVFoundation;

@interface KZPSampleBouncer : NSObject

@property (strong, nonatomic, readonly) NSURL *bounceURL;

- (instancetype)initWithDrumSurfaceButton:(KZPDrumSurfaceButton *)drumSurfaceButton;
- (void)bounceWithCompletion:(void (^)())completion;

@end
