//
//  KZPAutoSamplerViewController.m
//  KZPAutoSamplerClient
//
//  Created by Matt Rankin on 17/08/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPAutoSamplerViewController.h"
#import "KZPServerDiscoveryService.h"
#import "KZPServerQueryService.h"
#import "KZPQueueViewController.h"
#import "KZPStatusBarViewController.h"
#import "KZPFunctionsCollectionViewController.h"
#import "KZPDrumSurfaceCollectionViewController.h"
#import "KZPSamplePackExporter.h"
#import "MBProgressHUD.h"

@interface KZPAutoSamplerViewController () <KZPFunctionsDelegate>

@property (strong, nonatomic) KZPStatusBarViewController *statusBarViewController;
@property (strong, nonatomic) KZPQueueViewController *queueViewController;
@property (strong, nonatomic) KZPDrumSurfaceCollectionViewController *drumSurfaceViewController;
@property (strong, nonatomic) KZPFunctionsCollectionViewController *functionsViewController;
@property (strong, nonatomic) KZPSamplePackExporter *samplePackExporter;

@end

@implementation KZPAutoSamplerViewController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *embeddedViewController = segue.destinationViewController;
    
    if ([embeddedViewController isKindOfClass:[KZPQueueViewController class]]) {
        _queueViewController = (KZPQueueViewController *)embeddedViewController;
    }
    if ([embeddedViewController isKindOfClass:[KZPFunctionsCollectionViewController class]]) {
        _functionsViewController = (KZPFunctionsCollectionViewController *)embeddedViewController;
        _functionsViewController.delegate = self;
    }
    if ([embeddedViewController isKindOfClass:[KZPDrumSurfaceCollectionViewController class]]) {
        _drumSurfaceViewController = (KZPDrumSurfaceCollectionViewController *)embeddedViewController;
    }
    if ([embeddedViewController isKindOfClass:[KZPStatusBarViewController class]]) {
        _statusBarViewController = (KZPStatusBarViewController *)embeddedViewController;
    }
}


#pragma mark - <KZPFunctionsDelegate>

// TODO: is there are 'more correct' way to handle this through the shared interaction state

- (void)exportFunctionSelected
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _samplePackExporter = [[KZPSamplePackExporter alloc] initWithHUD:hud];
    NSArray *sampleBouncers = [_drumSurfaceViewController generateSampleBouncers];
    [_samplePackExporter bounceAndExportSamples:sampleBouncers];
}

- (void)resetFunctionSelected
{
    [_drumSurfaceViewController resetAllButtons];
}

@end
