//
//  KZPReverbEffect.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 6/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPReverbEffect.h"

@interface KZPReverbEffect ()

@property (strong, nonatomic) AVAudioUnitReverb *reverbUnitNode;

@end

@implementation KZPReverbEffect

- (instancetype)init
{
    self = [super init];
    if (self) {
        AudioComponentDescription reverbComponentDescription;
        reverbComponentDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
        reverbComponentDescription.componentType = kAudioUnitType_Effect;
        reverbComponentDescription.componentSubType = kAudioUnitSubType_Reverb2;
        _reverbUnitNode = [[AVAudioUnitReverb alloc] initWithAudioComponentDescription:reverbComponentDescription];
    }
    return self;
}

- (float)continuousValue
{
    return _reverbUnitNode.wetDryMix / 100;
}

- (void)applyContinuousValue:(float)value
{
    [_reverbUnitNode setWetDryMix:value * 100];
    
    float reverbTime = value * MAX_REVERB_TIME;
    AudioUnitSetParameter([_reverbUnitNode audioUnit], kReverb2Param_DecayTimeAt0Hz, kAudioUnitScope_Global, 0, reverbTime, 0);
    AudioUnitSetParameter([_reverbUnitNode audioUnit], kReverb2Param_DecayTimeAtNyquist, kAudioUnitScope_Global, 0, reverbTime, 0);
}

- (AVAudioUnit *)audioNode
{
    return _reverbUnitNode;
}

@end
