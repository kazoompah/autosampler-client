//
//  KZPDrumPadButton.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 20/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KZPDrumSurfaceCollectionViewCell.h"
#import "KZPPerformancePrefsViewController.h"
#import "KZPSample.h"
#import "KZPEffectsChain.h"

@class KZPDrumSurfaceButton;
@protocol KZPDrumSurfaceButtonDelegate <NSObject>

- (void)presentViewController:(UIViewController *)viewController;
- (void)drumSurfaceButtonWasTriggered:(KZPDrumSurfaceButton *)drumSurfaceButton;

@end

@interface KZPDrumSurfaceButton : NSObject <KZPSampleDelegate>

// Reason this is public is because the views themselves can be dynamically reassigned to
// existing buttons if the surface is resized. Buttons are allocated when views become visible or a saved surface
// refers to them, BUT they are not released when views disappear. They are remembered for later. So a view
// which becomes visible is assigned to an existing button for its new location if possible.
@property (weak, nonatomic) KZPDrumSurfaceCollectionViewCell *drumSurfaceButtonView;

@property (weak, nonatomic) id<KZPDrumSurfaceButtonDelegate> delegate;
@property (strong, nonatomic, readonly) KZPSample *sample;
@property (strong, nonatomic, readonly) KZPEffectsChain *effectsChain;

+ (NSString *)drumSurfaceLocationKeyWithLocation:(CGPoint)drumSurfaceLocation;

- (instancetype)initWithDrumSurfaceLocation:(CGPoint)drumSurfaceLocation
                        savedDrumButtonInfo:(NSDictionary *)savedDrumButtonInfo;

- (NSString *)label;
- (void)reset;

@property (nonatomic, readonly) KZPDrumSurfacePerformanceType performanceType;
@property (nonatomic, readonly) BOOL playsExclusive;

@end
