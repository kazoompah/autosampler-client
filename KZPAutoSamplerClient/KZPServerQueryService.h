//
//  KZPServerQueryService.h
//  KZPAutoSamplerClient
//
//  Created by Matt Rankin on 17/08/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KZPServerQueryServiceDelegate <NSObject>

- (void)queryServiceLostConnection;
- (void)queryServiceHasConnection;
- (void)queryServiceReceivedSampleListUpdate:(NSArray *)sampleList;

@end

@interface KZPServerQueryService : NSObject

@property (weak, nonatomic) id<KZPServerQueryServiceDelegate> delegate;

@property (nonatomic, strong) NSString *remoteQueryPath;

- (void)startPolling;
- (void)stopPolling;

@end
