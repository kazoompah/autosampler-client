//
//  KZPDrumSurfaceCollectionViewCell.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 20/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FDWaveformView-Swift.h"

typedef enum {
    KZPDrumSurfaceButtonStateInactive,
    KZPDrumSurfaceButtonStateLoading,
    KZPDrumSurfaceButtonStateActive,
    KZPDrumSurfaceButtonStateHighlighted,
    KZPDrumSurfaceButtonStateEditing,
    KZPDrumSurfaceButtonStateError,
} KZPDrumSurfaceButtonState;

@protocol KZPDrumSurfaceCellDelegate <NSObject>

- (void)drumSurfaceButtonEngaged;
- (void)drumSurfaceButtonReleased;

@end

@interface KZPDrumSurfaceCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) id<KZPDrumSurfaceCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *padImage;
@property (weak, nonatomic) IBOutlet UILabel *padLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet FDWaveformView *waveformView;
@property (weak, nonatomic) IBOutlet UIImageView *performanceStyleIndicator1;
@property (weak, nonatomic) IBOutlet UIImageView *performanceStyleIndicator2;
@property (weak, nonatomic) IBOutlet UIImageView *performanceStyleExclusiveIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *effectsIndicator;

- (void)displayState:(KZPDrumSurfaceButtonState)buttonState;
- (void)displayWaveformForAudioFileURL:(NSURL *)audioFileURL;

- (void)updatePerformanceIndicatorsWithLoop:(BOOL)loop hold:(BOOL)hold exclusive:(BOOL)exclusive;
- (void)showEffectsIndicator:(BOOL)show;

@end
