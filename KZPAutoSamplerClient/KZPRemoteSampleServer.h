//
//  KZPRemoteSampleServer.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 26/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KZPFileDownloader.h"
#import "KZPFileUploader.h"

@protocol KZPServerQueryDelegate <NSObject>

- (void)receiveSampleList:(NSArray *)sampleList;

@end

@protocol KZPServerConnectionDelegate <NSObject>

- (void)connectionWasEstablished;
- (void)connectionWasMaintained;
- (void)connectionWasPaused;
- (void)connectionWarningWithMessage:(NSString *)warningMessage;
- (void)connectionFailedWithMessage:(NSString *)failureMessage;

@end

@interface KZPRemoteSampleServer : NSObject

+ (KZPRemoteSampleServer *)sharedServer;
- (void)start;

- (void)startQuerying;
- (void)pauseQuerying;

- (KZPFileDownloader *)fileDownloaderForSampleID:(NSString *)sampleID;
- (KZPFileUploader *)fileUploaderForFilename:(NSString *)filename extension:(NSString *)extension;

// TODO: does the setting of these trigger the start of the network activity?
// or does it begin before the views are loaded and get cached in the meantime?

@property (weak, nonatomic) id<KZPServerQueryDelegate> queryDelegate;
@property (weak, nonatomic) id<KZPServerConnectionDelegate> connectionDelegate;

@end
