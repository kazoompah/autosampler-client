//
//  KZPSamplePacker.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 2/11/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KZPSamplePacker : NSObject

- (BOOL)packSamples:(NSArray *)sampleURLs withCollectionName:(NSString *)samplePackName;

@end
