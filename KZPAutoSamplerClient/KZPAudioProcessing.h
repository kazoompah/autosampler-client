//
//  KZPAudioProcessing.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 2/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

@import AVFoundation;

@interface KZPAudioProcessing : NSObject

+ (void)reverseSamples:(Float32 *)samples nSamples:(UInt32)nSamples;
+ (void)correctBiasForSamples:(Float32 *)samples nSamples:(UInt32)nSamples;
+ (void)normalizeSamples:(Float32 *)samples nSamples:(UInt32)nSamples;

@end
