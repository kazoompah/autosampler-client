//
//  KZPSamplePackExporter.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 2/11/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPSamplePackExporter.h"
#import "KZPSamplePacker.h"
#import "KZPFileUploader.h"
#import "KZPRemoteSampleServer.h"
#import "KZPSampleBouncer.h"
#import "KZPAudioEngine.h"

@interface KZPSamplePackExporter () <KZPFileUploaderDelegate>

@property (weak, nonatomic) MBProgressHUD *hud;

@property (strong, nonatomic) KZPSamplePacker *samplePacker;
@property (strong, nonatomic) KZPFileUploader *fileUploader;
@property (strong, nonatomic) NSString *samplePackName;
@property (strong, nonatomic) NSMutableArray *sampleBouncers;
@property (strong, nonatomic) NSMutableArray *bouncedSampleURLs;

@end

@implementation KZPSamplePackExporter

- (instancetype)initWithHUD:(MBProgressHUD *)hud
{
    self = [super init];
    if (self) {
        _hud = hud;
        _samplePackName = @"default";
    }
    return self;
}

- (void)bounceAndExportSamples:(NSArray *)sampleBouncers
{
    _sampleBouncers = [NSMutableArray arrayWithArray:sampleBouncers];
    [self getSamplePackName];
}

- (void)getSamplePackName
{
    UIAlertController *samplePackNameAlert = [UIAlertController alertControllerWithTitle:@"Sample Pack Name" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [samplePackNameAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Enter name...";
        [textField becomeFirstResponder];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [_hud hideAnimated:YES];
    }];
    
    UIAlertAction *acceptAction = [UIAlertAction actionWithTitle:@"Generate" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _samplePackName = samplePackNameAlert.textFields[0].text;
        _bouncedSampleURLs = [NSMutableArray array];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[KZPAudioEngine sharedEngine] mute];
            [self bounceSamples];
        });
    }];
    
    [samplePackNameAlert addAction:cancelAction];
    [samplePackNameAlert addAction:acceptAction];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:samplePackNameAlert
                                                                                     animated:YES
                                                                                   completion:^{}];
}

- (void)packAndExport
{
    if (![self packBouncedSamples]) {
        return;
    }
    
    [self exportSamples];
}

- (void)bounceSamples
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _hud.mode = MBProgressHUDModeIndeterminate;
        _hud.label.text = @"Bouncing...";
    });
    
    if ([_sampleBouncers count] > 0) {
        [self bounceNextSample];
    } else {
        [[KZPAudioEngine sharedEngine] unmute];
        [self packAndExport];
    }
}

- (void)bounceNextSample
{
    KZPSampleBouncer *bouncer = [_sampleBouncers lastObject];
    NSURL *bouncedURL = [bouncer bounceURL];
    [bouncer bounceWithCompletion:^{
        [_bouncedSampleURLs addObject:bouncedURL];
        [_sampleBouncers removeLastObject];
        [self bounceSamples];
    }];
}

- (BOOL)packBouncedSamples
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _hud.mode = MBProgressHUDModeIndeterminate;
        _hud.label.text = @"Packing...";
    });
    
    _samplePacker = [[KZPSamplePacker alloc] init];
    
    // TODO: get back file URL?
    
    if (![_samplePacker packSamples:_bouncedSampleURLs withCollectionName:_samplePackName]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _hud.label.text = @"Packing failed";
            [_hud hideAnimated:YES afterDelay:2.0];
        });
        return NO;
    }
    else {
        // delete all the _bouncedSampleURLs
    }
    return YES;
}

- (void)exportSamples
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _hud.mode = MBProgressHUDModeAnnularDeterminate;
        _hud.label.text = @"Uploading...";
    });
    
    _fileUploader = [[KZPRemoteSampleServer sharedServer] fileUploaderForFilename:_samplePackName
                                                                        extension:@"zip"];
    _fileUploader.delegate = self;
    [_fileUploader upload];
}


#pragma mark - <KZPFileUploaderDelegate>


- (void)fileUploadComplete
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _hud.label.text = @"Done";
        [_hud hideAnimated:YES afterDelay:1.0];
    });
}

- (void)fileUploadFailed
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _hud.label.text = @"Upload failed";
        [_hud hideAnimated:YES afterDelay:2.0];
    });
}

- (void)fileUploadProgress:(double)progress
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _hud.progress = progress;
    });
}

@end
