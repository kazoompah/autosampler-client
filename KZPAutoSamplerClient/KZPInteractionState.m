//
//  KZPUsageMode.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 20/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPInteractionState.h"

static KZPInteractionState *sharedInstance;

@interface KZPInteractionState ()

@property (weak, nonatomic) id temporaryObject;
@property (strong, nonatomic) NSMutableArray *interactionObservers;

@end

@implementation KZPInteractionState

+ (KZPInteractionState *)sharedInteractionState
{
    if (!sharedInstance) sharedInstance = [[KZPInteractionState alloc] init];
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _interactionObservers = [NSMutableArray array];
    }
    return self;
}

- (void)notifyObservers
{
    for (id<KZPInteractionStateObserver> observer in _interactionObservers) {
        [observer interactionStateDidChange];
    }
}

- (void)registerInteractionObserver:(id<KZPInteractionStateObserver>)observer
{
    [_interactionObservers addObject:observer];
}

- (void)unregisterInteractionObserver:(id<KZPInteractionStateObserver>)observer
{
    [_interactionObservers removeObject:observer];
}


#pragma mark - State Transitions: Drum Surface


- (void)provideTemporaryObject:(id)temporaryObject
{
    _temporaryObject = temporaryObject;
}

- (id)fetchTemporaryObjectOfType:(Class)expectedType
{
    if ([_temporaryObject isKindOfClass:expectedType]) {
        return _temporaryObject;
    }
    return nil;
}

- (void)removeTemporaryObject
{
    _temporaryObject = nil;
}

- (void)drumSurfaceDidLoad
{
    _drumSurfaceUsageMode = KZPDrumSurfaceUsageModePerformance;
}


#pragma mark - State Transitions: Functions


- (void)queueSampleSelectionEnded
{
    _temporaryObject = nil;
    _appUsageMode = KZPAppUsageModePerformance;
    _drumSurfaceUsageMode = KZPDrumSurfaceUsageModePerformance;
    [self notifyObservers];
}

- (void)queueSampleWasSelected
{
    _appUsageMode = KZPAppUsageModeSampleQueue;
    _drumSurfaceUsageMode = KZPDrumSurfaceUsageModeQueueAssignment;
    [self notifyObservers];
}

- (void)functionSelectedWithDrumSurfaceUsageMode:(KZPDrumSurfaceUsageMode)drumSurfaceUsageMode
{
    _appUsageMode = KZPAppUsageModeFunctions;
    _drumSurfaceUsageMode = drumSurfaceUsageMode;
    [self notifyObservers];
}

- (void)functionDeselected
{
    _appUsageMode = KZPAppUsageModePerformance;
    _drumSurfaceUsageMode = KZPDrumSurfaceUsageModePerformance;
    _temporaryObject = nil;
    [self notifyObservers];
}

@end
