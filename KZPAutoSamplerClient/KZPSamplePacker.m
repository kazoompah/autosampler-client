//
//  KZPSamplePacker.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 2/11/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPSamplePacker.h"
#import "SSZipArchive.h"
#import "KZPSampleFileManager.h"

@implementation KZPSamplePacker

- (BOOL)packSamples:(NSArray *)sampleURLs withCollectionName:(NSString *)samplePackName
{
    NSMutableArray *samplePaths = [NSMutableArray array];
    for (NSURL *sampleURL in sampleURLs) {
        [samplePaths addObject:[sampleURL path]];
    }
    
    NSString *documentsPath = [[KZPSampleFileManager sharedFileManager] documentsPath];
    NSString *samplePackFileName = [samplePackName stringByAppendingPathExtension:@"zip"];
    NSString *samplePackPath = [documentsPath stringByAppendingPathComponent:samplePackFileName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:samplePackPath]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:samplePackPath error:&error];
        if (error) {
            NSLog(@"Failed to delete previous sample pack: %@: %@", samplePackFileName, [error localizedDescription]);
        } else {
            NSLog(@"Deleted existing %@", samplePackFileName);
        }
    }
    
    return [SSZipArchive createZipFileAtPath:samplePackPath withFilesAtPaths:samplePaths];
}

@end
