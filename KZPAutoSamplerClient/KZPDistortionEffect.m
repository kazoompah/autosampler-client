//
//  KZPDistortionEffect.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 6/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPDistortionEffect.h"

@interface KZPDistortionEffect ()

@property (strong, nonatomic) AVAudioUnitDistortion *distortionUnit;

@end

@implementation KZPDistortionEffect

- (instancetype)init
{
    self = [super init];
    if (self) {
        AudioComponentDescription distortionComponentDescription;
        distortionComponentDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
        distortionComponentDescription.componentType = kAudioUnitType_Effect;
        distortionComponentDescription.componentSubType = kAudioUnitSubType_Distortion;
        _distortionUnit = [[AVAudioUnitDistortion alloc] initWithAudioComponentDescription:distortionComponentDescription];
        [_distortionUnit loadFactoryPreset:AVAudioUnitDistortionPresetMultiDistortedCubed];
    }
    return self;
}

- (void)applyContinuousValue:(float)value
{
    _distortionUnit.wetDryMix = value * 100;
}

- (float)continuousValue
{
    return _distortionUnit.wetDryMix / 100;
}

- (AVAudioUnit *)audioNode
{
    return _distortionUnit;
}

@end
