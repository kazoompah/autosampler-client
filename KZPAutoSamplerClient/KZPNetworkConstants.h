//
//  KZPNetworkConstants.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 26/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#ifndef KZPNetworkConstants_h
#define KZPNetworkConstants_h

#define SERVER_DISCOVERY_BROADCAST          @"255.255.255.255"

#define SERVER_DISCOVERY_UDP_PORT           61670
#define SERVER_QUERY_TCP_PORT               61671

#define SERVER_DISCOVERY_INTERVAL           0.3
#define SERVER_DISCOVERY_TIMEOUT            3.0

#define SERVER_QUERY_INTERVAL               1.0
#define SERVER_QUERY_TIMEOUT                2.0
#define SERVER_POST_TIMEOUT                 5.0

#define kSERVER_PATH_SAMPLES_SINCE          @"samples-since"
#define kSERVER_PATH_SAMPLES                @"samples"
#define kSERVER_PATH_SAMPLE_PACKS           @"sample-packs"

#define kSERVER_RESPONSE_LAST_TIMESTAMP     @"last-timestamp"
#define kSERVER_RESPONSE_SAMPLES            @"samples"

#define kConnectionStatus_ServerConnectionLost      @"Connection lost"
#define kConnectionStatus_ServerNotFound            @"Server not found"
#define kConnectionStatus_NetworkNotPresent         @"No network"
#define kConnectionStatus_DownloadFailed            @"Download failed"

#endif /* KZPNetworkConstants_h */
