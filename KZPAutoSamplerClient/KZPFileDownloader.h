//
//  KZPFileDownloader.h
//  KZPAutoSamplerClient
//
//  Created by Matt Rankin on 31/08/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KZPFileDownloaderDelegate <NSObject>

- (void)fileDownloadComplete;
- (void)fileDownloadFailed;
- (void)fileDownloadProgress:(double)progress;

@end

@interface KZPFileDownloader : NSObject

@property (weak, nonatomic) id<KZPFileDownloaderDelegate> delegate;

@property (strong, nonatomic) NSString *remoteFilePath;
@property (strong, nonatomic) NSString *fileName;
@property (strong, nonatomic, readonly) NSString *localFilePath;
@property (strong, nonatomic, readonly) NSURL *localFileURL;

- (void)download;

@end
