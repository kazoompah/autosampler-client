//
//  KZPSampleReceiver.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 28/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPSampleReceiver.h"
#import "KZPRemoteSampleServer.h"

@interface KZPSampleReceiver () <KZPServerQueryDelegate>

@property (strong, atomic) NSMutableArray *incomingSampleList;

@property (nonatomic) BOOL paused;

@end

// TODO: check thread safety

@implementation KZPSampleReceiver

- (instancetype)init
{
    self = [super init];
    if (self) {
        _incomingSampleList = [NSMutableArray array];
        [[KZPRemoteSampleServer sharedServer] setQueryDelegate:self];
    }
    return self;
}

- (NSString *)nextSampleID
{
    if (_paused) return nil;
    
    NSString *sampleID = [_incomingSampleList lastObject];
    if (sampleID) {
        [_incomingSampleList removeLastObject];
        if ([self sampleIsValid:sampleID]) {
            return sampleID;
        } else {
            [self reset];
        }
    }
    return nil;
}

- (BOOL)sampleIsValid:(NSString *)sampleID
{
    NSTimeInterval now_ms = [NSDate timeIntervalSinceReferenceDate] * 1000;
    NSTimeInterval sampleTimestamp_ms = [sampleID doubleValue];
    NSTimeInterval sampleLifetime_ms = INCOMING_SAMPLE_LIFETIME * 1000;
    return (sampleTimestamp_ms + sampleLifetime_ms > now_ms);
}
        
- (void)reset
{
    [_incomingSampleList removeAllObjects];
}

- (void)stopReceiving
{
    [[KZPRemoteSampleServer sharedServer] pauseQuerying];
    _paused = YES;
}

- (void)startReceiving
{
    [[KZPRemoteSampleServer sharedServer] startQuerying];
    _paused = NO;
}


#pragma mark - <KZPServerQueryDelegate>


- (void)receiveSampleList:(NSArray *)sampleList
{
    [_incomingSampleList addObjectsFromArray:sampleList];
    [_delegate newSamplesReceived];
}

@end
