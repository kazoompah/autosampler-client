//
//  KZPPitchShiftEffect.h
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 6/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPAudioEffect.h"

@interface KZPPitchShiftEffect : KZPAudioEffect

@end
