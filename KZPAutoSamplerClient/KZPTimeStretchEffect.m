//
//  KZPTimeStretchEffect.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 6/10/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPTimeStretchEffect.h"

@interface KZPTimeStretchEffect ()

@property (strong, nonatomic) AVAudioUnitTimePitch *stretchUnit;

@end

@implementation KZPTimeStretchEffect

- (instancetype)init
{
    self = [super init];
    if (self) {
        AudioComponentDescription stretchComponentDescription;
        stretchComponentDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
        stretchComponentDescription.componentType = kAudioUnitType_FormatConverter;
        stretchComponentDescription.componentSubType = kAudioUnitSubType_NewTimePitch;
        _stretchUnit = [[AVAudioUnitTimePitch alloc] initWithAudioComponentDescription:stretchComponentDescription];
    }
    return self;
}

- (void)applyContinuousValue:(float)value
{
    _stretchUnit.rate = powf(2, (value - 0.5) / 0.25);
}

- (float)continuousValue
{
    return log2f(_stretchUnit.rate) / 4 + 0.5;
}

- (AVAudioUnit *)audioNode
{
    return _stretchUnit;
}

@end
