//
//  KZPUDPDiscoveryService.m
//  KZPAutoSamplerClient
//
//  Created by Matt Rankin on 30/08/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPUDPDiscoveryService.h"
#import <arpa/inet.h>
#import "NSString+functions.h"
#import "SSNetworkInfo.h"
#import "KZPNetworkConstants.h"

@import UIKit;

@interface KZPUDPDiscoveryService ()

@property (strong, nonatomic) GCDAsyncUdpSocket *udpSocket;
@property (strong, nonatomic) NSString *token;
@property (nonatomic) BOOL cancelled;

@end

@implementation KZPUDPDiscoveryService

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self listenForUDP];
    }
    return self;
}

- (void)listenForUDP
{
    self.udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    [self.udpSocket enableBroadcast:YES error:nil];
    
    NSError *error = nil;
    if (![self.udpSocket bindToPort:SERVER_DISCOVERY_UDP_PORT error:&error]) {
        NSLog(@"Error binding: %@", [error description]);
    }
    if (![self.udpSocket beginReceiving:&error]) {
        NSLog(@"Error receiving: %@", [error description]);
    }
}

- (void)sendDiscoveryBroadcast
{
    self.cancelled = NO;
    
    // Send in bursts to compensate for occasional UDP packet loss
    for (int i = 0; i < 3; i++) {
        [self.udpSocket sendData:[[[UIDevice currentDevice] name] dataUsingEncoding:NSUTF8StringEncoding]
                          toHost:SERVER_DISCOVERY_BROADCAST
                            port:SERVER_DISCOVERY_UDP_PORT
                     withTimeout:-1
                             tag:1];
    }
}

- (void)cancel
{
    self.cancelled = YES;
}


#pragma mark - GCDAsyncUdpSocketDelegate -


- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext
{
    if (self.cancelled) return;
    
    NSString *hostIP = [self addressWithData:address];
    
    if (hostIP && ![hostIP isEqualToString:[SSNetworkInfo wiFiIPAddress]]) {
        BOOL serverHasMinimumVersion = [self serverHasMinimumVersion:data];
        if (serverHasMinimumVersion) {
            [self.delegate discoverySucceededWithIP:hostIP];
        } else {
            NSString *message = [NSString stringWithFormat:@"Upgrade server to %@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"KZPMinServerVersion"]];
            [self.delegate discoveryFailedWithMessage:message abort:NO];
        }
    }
}

- (BOOL)serverHasMinimumVersion:(NSData *)data
{
    NSString *serverVersionString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *minimumServerVersionString = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"KZPMinServerVersion"];
    NSArray *serverVersionStringComponents = [serverVersionString componentsSeparatedByString:@"."];
    NSArray *minimumServerVersionStringComponents = [minimumServerVersionString componentsSeparatedByString:@"."];
    
    if ([serverVersionStringComponents count] != 3) return NO;
    
    if ([serverVersionStringComponents[0] intValue] > [minimumServerVersionStringComponents[0] intValue]) {
        return YES;
    } else if ([serverVersionStringComponents[0] intValue] < [minimumServerVersionStringComponents[0] intValue]) {
        return NO;
    }
    
    if ([serverVersionStringComponents[1] intValue] > [minimumServerVersionStringComponents[1] intValue]) {
        return YES;
    } else if ([serverVersionStringComponents[1] intValue] < [minimumServerVersionStringComponents[1] intValue]) {
        return NO;
    }
    
    if ([serverVersionStringComponents[2] intValue] > [minimumServerVersionStringComponents[2] intValue]) {
        return YES;
    } else if ([serverVersionStringComponents[2] intValue] < [minimumServerVersionStringComponents[2] intValue]) {
        return NO;
    }
    
    return YES;
}


#pragma mark - Utility -


- (NSString *)addressWithData:(NSData *)address
{
    struct sockaddr_in socket = (*(struct sockaddr_in *)([address bytes]));
    struct in_addr ip_addr = socket.sin_addr;
    if (ip_addr.s_addr > 0) {
        NSString *ipString = [NSString stringWithCString:inet_ntoa(ip_addr) encoding:NSUTF8StringEncoding];
        return ipString;
    }
    return nil;
}


@end
