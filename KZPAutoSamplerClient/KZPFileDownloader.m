//
//  KZPFileDownloader.m
//  KZPAutoSamplerClient
//
//  Created by Matt Rankin on 31/08/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPFileDownloader.h"
#import "AFNetworking.h"
#import "KZPSampleFileManager.h"

@implementation KZPFileDownloader

- (void)setFileName:(NSString *)fileName
{
    _fileName = fileName;
    [self generateFilePath];
}

- (void)generateFilePath
{
    NSString *documents = [[KZPSampleFileManager sharedFileManager] documentsPath];
    
    if (self.fileName) {
        _localFilePath = [documents stringByAppendingPathComponent:_fileName];
        _localFileURL = [NSURL fileURLWithPath:_localFilePath];
    }
}

- (void)download
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_remoteFilePath]];
    
    [manager setDownloadTaskDidWriteDataBlock:^(NSURLSession * _Nonnull session, NSURLSessionDownloadTask * _Nonnull downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [self.delegate fileDownloadProgress:(double)totalBytesWritten/totalBytesExpectedToWrite];
        });
        
    }];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        [self removeExistingFile];
        return _localFileURL;
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if (!error) {
            [self.delegate fileDownloadComplete];
        } else {
            [self.delegate fileDownloadFailed];
        }
    }];
    
    [downloadTask resume];
}

- (void)removeExistingFile
{
    NSError *error;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:_localFilePath]) {
        [fileManager removeItemAtPath:_localFilePath error:&error];
    }
    
    if (error) {
        NSLog(@"ERROR: %@", error);
    }
}

@end