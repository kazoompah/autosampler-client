//
//  KZPEffectsChain.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 20/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPEffectsChain.h"
#import "KZPAudioEffect+Creator.h"
#import "KZPAudioEngine.h"

@interface KZPEffectsChain ()

@property (strong, nonatomic) NSMutableDictionary *effects;

@property (strong, nonatomic) KZPAudioEffect *inputEffect;
@property (strong, nonatomic) KZPAudioEffect *outputEffect;

@end

@implementation KZPEffectsChain


- (void)copyEffectsChain:(KZPEffectsChain *)effectsChain withCompletion:(void (^)(void))completionBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (NSString *effectID in [self activeEffects]) {
            [self deactivateEffect:effectID withoutReconnecting:YES];
        }
        for (NSString *effectID in [effectsChain activeEffects]) {
            [self activateEffect:effectID withoutReconnecting:YES];
            [self applyContinuousValue:[effectsChain continuousValueForEffect:effectID] toEffect:effectID];
        }
        [_delegate effectsChainNeedsReconnecting];
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock();
        });
    });
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _effects = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)setFormat:(AVAudioFormat *)format
{
    AVAudioFormat *previousFormat = _format;
    _format = format;
    
    if (previousFormat != format) {
        [self reconfigureChain];
    }
}

- (AVAudioNode *)effectsChainInputNode
{
    return [_inputEffect audioNode];
}

- (AVAudioNode *)effectsChainOutputNode
{
    return [_outputEffect audioNode];
}

- (NSArray *)activeEffects
{
    return [_effects allKeys];
}

- (NSDictionary *)currentSettings
{
    NSMutableDictionary *settings = [NSMutableDictionary dictionary];
    for (NSString *effectID in [self activeEffects]) {
        settings[effectID] = @([self continuousValueForEffect:effectID]);
    }
    return settings;
}

- (void)loadWithDictionary:(NSDictionary *)effectSettings withCompletion:(void (^)(void))completionBlock
{
    if (!effectSettings) { completionBlock(); return; }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (NSString *effectID in effectSettings) {
            [self activateEffect:effectID withoutReconnecting:YES];
            [self applyContinuousValue:[effectSettings[effectID] floatValue] toEffect:effectID];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [_delegate effectsChainNeedsReconnecting];
            completionBlock();
        });
    });
}

- (float)continuousValueForEffect:(NSString *)effectID
{
    KZPAudioEffect *effect = _effects[effectID];
    return [effect continuousValue];
}

- (void)applyContinuousValue:(float)value toEffect:(NSString *)effectID
{
    KZPAudioEffect *effect = _effects[effectID];
    [effect applyContinuousValue:value];
}

- (void)activateEffect:(NSString *)effectID withoutReconnecting:(BOOL)noReconnection
{
    if (!_effects[effectID]) {
        KZPAudioEffect *effect = [KZPAudioEffect effectWithEffectID:effectID];
        _effects[effectID] = effect;
        
        KZPAudioEffect *previousInputEffect = _inputEffect;
        KZPAudioEffect *previousOutputEffect = _outputEffect;
        
        [self insertEffectIntoChain:effect];
        
        if ((previousInputEffect != _inputEffect || previousOutputEffect != _outputEffect) && !noReconnection) {
            [_delegate effectsChainNeedsReconnecting];
        }
    }
}

- (void)insertEffectIntoChain:(KZPAudioEffect *)effect
{
    if (!_inputEffect || !_outputEffect) {
        _inputEffect = effect;
        _outputEffect = effect;
        [[KZPAudioEngine sharedEngine] connectEffect:effect toEffect:nil format:_format];
    }
    
    KZPAudioEffect *currentEffect = _inputEffect;
    while (currentEffect.downstreamEffect) {
        currentEffect = currentEffect.downstreamEffect;
    };
    
    effect.upstreamEffect = currentEffect;
    effect.upstreamEffect.downstreamEffect = effect;
    
    [[KZPAudioEngine sharedEngine] connectEffect:effect.upstreamEffect toEffect:effect format:_format];  // May not actually connect them
    
    _outputEffect = effect;
}

- (void)reconfigureChain
{
    KZPAudioEffect *inputEffect = _inputEffect;
    while (inputEffect.downstreamEffect) {
        [[KZPAudioEngine sharedEngine] connectEffect:inputEffect
                                            toEffect:inputEffect.downstreamEffect
                                              format:_format];
        inputEffect = inputEffect.downstreamEffect;
    }
}

- (void)deactivateEffect:(NSString *)effectID withoutReconnecting:(BOOL)noReconnection
{
    KZPAudioEffect *previousInputEffect = _inputEffect;
    KZPAudioEffect *previousOutputEffect = _outputEffect;
    
    KZPAudioEffect *effect = _effects[effectID];
    [self removeEffectFromChain:effect];
    
    if ((previousInputEffect != _inputEffect || previousOutputEffect != _outputEffect) && !noReconnection) {
        [_delegate effectsChainNeedsReconnecting];
    }
    
    [_effects removeObjectForKey:effectID];
}

- (void)removeEffectFromChain:(KZPAudioEffect *)effect
{
    [[KZPAudioEngine sharedEngine] detachEffect:effect];
    
    [[KZPAudioEngine sharedEngine] connectEffect:effect.upstreamEffect
                                        toEffect:effect.downstreamEffect
                                          format:_format];
    
    if (effect == _inputEffect) {
        _inputEffect = effect.downstreamEffect;
    }
    
    if (effect == _outputEffect) {
        _outputEffect = effect.upstreamEffect;
    }
}

- (void)dealloc
{
    for (NSString *effectID in [self activeEffects]) {
        [self deactivateEffect:effectID withoutReconnecting:YES];
    }
}

@end
