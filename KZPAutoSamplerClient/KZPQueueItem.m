//
//  KZPQueueItem.m
//  KZPAutoSamplerClient
//
//  Created by Matthew Rankin on 19/09/2016.
//  Copyright © 2016 Sudoseng. All rights reserved.
//

#import "KZPQueueItem.h"
#import "KZPFileDownloader.h"
#import "KZPRemoteSampleServer.h"
#import "KZPAudioEngine.h"
#import "KZPInteractionState.h"
#import "KZPAudioEngine.h"


@interface KZPQueueItem () <KZPFileDownloaderDelegate, KZPInteractionStateObserver, KZPQueueItemTableViewCellDelegate, KZPSampleDelegate>

@property (strong, nonatomic) KZPFileDownloader *downloader;

@end

@implementation KZPQueueItem

- (instancetype)initWithSampleID:(NSString *)sampleID
{
    self = [super init];
    if (self) {
        [[KZPInteractionState sharedInteractionState] registerInteractionObserver:self];
        _downloader = [[KZPRemoteSampleServer sharedServer] fileDownloaderForSampleID:sampleID];
        _downloader.delegate = self;
        [_downloader download];
    }
    return self;
}

- (void)setQueueItemView:(KZPQueueItemTableViewCell *)queueItemView
{
    _queueItemView = queueItemView;
    [_queueItemView reset];
    _queueItemView.delegate = self;
}

- (void)requestDeletion
{
    [[KZPInteractionState sharedInteractionState] unregisterInteractionObserver:self];
    [_delegate queueItemRequestedDeletion:self];
}


#pragma mark - <KZPFileDownloaderDelegate>


- (void)fileDownloadComplete
{
    _sample = [KZPSample sampleWithFileURL:[NSURL fileURLWithPath:_downloader.localFilePath]];
    _sample.delegate = self;
    
    [_sample loadWithCompletion:^{
        [_queueItemView displayReadyState:YES];
        [_queueItemView displayWaveformForAudioFileURL:_sample.audioFileURL];
    } failure:^{
        [self requestDeletion];
    }];
}

- (void)fileDownloadProgress:(double)progress
{
    // call delegate if progress is relevant to the user
}

- (void)fileDownloadFailed
{
    [self requestDeletion];
}


#pragma mark - <KZPInteractionStateObserver>


- (void)interactionStateDidChange
{
    [_sample stopPlayback];
    if ([[KZPInteractionState sharedInteractionState] appUsageMode] == KZPAppUsageModeSampleQueue) {
        [[KZPAudioEngine sharedEngine] detachSample:_sample];
    }
    [_queueItemView displayStandbyState:NO];
}


#pragma mark - <KZPQueueItemTableViewCellDelegate>

/*
    The drum surface uses channels 1+, one channel per button, to feed sample buffers into the mixer.
    It doesn't make sense to allocate an indefinite number of channels to the queue items. Instead,
    the entire queue is assigned channel 0, which means each queue item must play exclusively.
 */
- (void)queueItemPlayButtonPressed
{
    [[KZPInteractionState sharedInteractionState] queueSampleWasSelected];
    [[KZPInteractionState sharedInteractionState] provideTemporaryObject:self];
    [_queueItemView displayStandbyState:YES];
    
    if (_sample.isLoaded) {
        [[KZPAudioEngine sharedEngine] activateSample:_sample onChannel:0 withEffectsChain:nil];
        [_sample restartPlaybackIgnoreLoop:YES];
    }
}


#pragma mark - <KZPSampleDelegate>


- (void)sampleDidStartPlaying
{
    [_queueItemView displayActiveState:YES];
}

- (void)sampleDidFinishPlaying
{
    [_queueItemView displayActiveState:NO];
}

@end
